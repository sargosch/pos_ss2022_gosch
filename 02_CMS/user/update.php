<!DOCTYPE html>
<html lang="de">
<?php
include "../config.php";
$pathStart = "../";
include "../views/helper/head.php";
?>

<body>

    <?php
    $pathToIndex = "../index.php";
    $pathToArticle = "../views/article/index.php";
    $pathToViewUser = "index.php";
    include "../views/helper/navbar.php";

    require_once('../models/Article.php');
    require_once('../models/User.php');

    $currentUser = User::get($_SESSION['user']);

    if (isset($_GET['uid'])) {
        $user = User::get($_GET['uid']);

        $_SESSION['currentUser'] = serialize($user);
    }


    if (isset($_POST['submit'])) {
        $user = unserialize($_SESSION['currentUser']);
        $user->setUname(isset($_POST['uname']) ? $_POST['uname'] : '');
        $user->setUpwhash(isset($_POST['password']) ? $_POST['password'] : '');

        $user->save();

        header("Location: view.php?uid=" . $user->getUid());
    }
    ?>

    <div class="container">
        <div class="row">
            <h2>Beitrag bearbeiten</h2>
        </div>

        <?php
        if ($currentUser->getUid() == $user->getUid() || $currentUser->getUid() == 1) {
        ?>
            <form class="form-horizontal" action="update.php" method="post">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="uname">Username *</label>
                            <input type="text" class="form-control" name="uname" maxlength="100" value="<?= $user->getUname() ?>">
                            <?php if (!empty($user->getErrors()['uname'])) : ?>
                                <div class="help-block"><?= $user->getErrors()['uname'] ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="password">Password *</label>
                            <input type="password" class="form-control" name="password" value="<?= $user->getUpwhash() ?>">
                            <?php if (!empty($user->getErrors()['upwhash'])) : ?>
                                <div class="help-block"><?= $user->getErrors()['upwhash'] ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
                    <a class="btn btn-default" href="javascript:history.back()">Abbruch</a>
                </div>
            </form>

        <?php
        } else {
        ?>
            <div class="row">
                <h3>Benutzer können nur vom Benutzer selbst bearbeitet werden!</h3>
            </div>
            <div class="row">
                <a href="javascript:history.back()" class="btn btn-danger" role="button"><span>Zurück</span></a>
            </div>
        <?php
        }
        ?>
    </div> <!-- /container -->
</body>

</html>