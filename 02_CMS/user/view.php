<!DOCTYPE html>
<html lang="de">
<?php
$pathStart = "../";
include "../config.php";
include "../views/helper/head.php";
?>

<body>

<?php
$pathToIndex = "../index.php";
$pathToArticle = "../views/article/index.php";
$pathToViewUser = "index.php";
include "../views/helper/navbar.php";

require_once("../models/Article.php");
require_once("../models/User.php");

$user = null;
$currentUser = User::get($_SESSION['user']);

if(isset($_GET['uid'])) {
    $user = User::get($_GET['uid']);
} else {
    $user = $currentUser;
}
?>

<div class="container">
    <h2>Benutzer anzeigen</h2>
    <p>
        <a class="btn btn-primary" href="update.php?uid=<?= $user->getUid() ?>">Aktualisieren</a>
        <a class="btn btn-default" href="javascript:history.back()">Zurück</a>
    </p>

    <?php
        if(isset($_SESSION['login']) && $_SESSION['login'] == true && $user != null) {
    ?>
    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>ID</th>
            <td><?= $user->getUid() ?></td>
        </tr>
        <tr>
            <th>Username</th>
            <td><?= $user->getUname() ?></td>
        </tr>
        <tr>
            <th>Passwort</th>
            <td><?= $user->getUpwhash() ?></td>
        </tr>
        </tbody>
    </table>
    <?php
        } else {
            ?>
            <div class="col-md-4">
                <h2>Benutzer</h2>
                <p>Bitte melden Sie sich an, um die Inhalte sehen zu können.</p>
            </div>
        <?php
        }
        ?>
</div> <!-- /container -->
</body>
</html>