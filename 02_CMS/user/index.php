<?php
include "../config.php";
?>

<!DOCTYPE html>
<html lang="de">
<?php
$pathStart = "../";
include "../views/helper/head.php";
?>

<body>

    <?php
    $pathToIndex = "../index.php";
    $pathToArticle = "../views/article/index.php";
    $pathToViewUser = "index.php";
    include "../views/helper/navbar.php";
    require_once('../models/Article.php');

    require_once('../models/User.php');

    $currentUser = User::get($_SESSION['user']);
    ?>

    <div class="container">
        <div class="row">
            <h2>Benutzer</h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($_SESSION['login']) && $_SESSION['login'] == true && $currentUser->getUid() == 1) {
                        foreach (User::getAll() as $key => $user) {
                            $user = User::get($user->getUid());
                    ?>
                            <tr>
                                <td><?= $user->getUname() ?></td>
                                <td><?= $user->getUpwhash() ?></td>
                                <td><a class="btn btn-info" href="view.php?uid=<?= $user->getUid() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a class="btn btn-primary" href="update.php?uid=<?= $user->getUid() ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
                                </td>
                            </tr>
                        <?php
                        }
                    } else if (isset($_SESSION['login']) && $_SESSION['login'] == true && $currentUser->getUid() != 1) {
                        $user = User::get($currentUser->getUid());
                        ?>
                        <tr>
                            <td><?= $user->getUname() ?></td>
                            <td><?= $user->getUpwhash() ?></td>
                            <td><a class="btn btn-info" href="view.php?uid=<?= $user->getUid() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a class="btn btn-primary" href="update.php?uid=<?= $user->getUid() ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
                            </td>
                        </tr>
                    <?php

                    } else {
                    ?>
                        <td>Fehler</td>
                        <td>Alle Benutzer können nur vom Admin eingesehen werden!</td>
                        <td></td>
                        <td></td>
                        <td>
                        </td>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->
</body>

</html>