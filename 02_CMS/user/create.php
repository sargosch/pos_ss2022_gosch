<!DOCTYPE html>
<html lang="de">
<?php
include "../config.php";
$pathStart = "../";
include "../views/helper/head.php";

require_once('../models/User.php');

$currentUser = User::get($_SESSION['user']);

$user = new User();

if (isset($_POST['create'])) {
    $user->setUname(isset($_POST['uname']) ? $_POST['uname'] : '');
    $user->setUpwhash(isset($_POST['password']) ? $_POST['password'] : '');

    if ($user->save()) {
        header("Location: view.php?uid=" . $user->getUid());
        exit();
    }
}
?>

<body>

    <?php
    $pathToIndex = "../index.php";
    $pathToArticle = "../views/article/index.php";
    $pathToViewUser = "index.php";
    include "../views/helper/navbar.php";
    ?>

    <div class="container">
        <div class="row">
            <h2>User erstellen</h2>
        </div>
        <?php
        if ($currentUser->getUid() == 1) {
        ?>
            <form class="form-horizontal" action="create.php" method="post">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="uname">Username *</label>
                            <input type="text" class="form-control" name="uname" maxlength="100" value="<?= $user->getUname() ?>">
                            <?php if (!empty($user->getErrors()['uname'])) : ?>
                                <div class="help-block"><?= $user->getErrors()['uname'] ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="password">Password *</label>
                            <input type="password" class="form-control" name="password" value="<?= $user->getUpwhash() ?>">
                            <?php if (!empty($user->getErrors()['upwhash'])) : ?>
                                <div class="help-block"><?= $user->getErrors()['upwhash'] ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="form-group">
                        <button type="submit" name="create" class="btn btn-success">Erstellen</button>
                        <a class="btn btn-default" href="javascript:history.back()">Abbruch</a>
                    </div>
                </div>
            </form>
        <?php
        } else {
        ?>
            <div class="row">
                <h3>Sie können nur als Admin neue Benutzer erstellen!</h3>
            </div>
            <div class="row">
                <a href="javascript:history.back()" class="btn btn-danger" role="button"><span>Zurück</span></a>
            </div>
        <?php
        }
        ?>
    </div> <!-- /container -->
</body>

</html>