<head>
    <meta charset="utf-8">
    <title>Awesome CMS</title>

    <link rel="shortcut icon" href="../../css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../css/favicon.ico" type="image/x-icon">

    <link href="<?= $pathStart ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $pathStart ?>css/index.css" rel="stylesheet">
    <script src="<?= $pathStart ?>js/jquery.min.js"></script>
    <script src="<?= $pathStart ?>js/bootstrap.min.js"></script>
</head>