<?php
$title = "Zimmerverwaltung";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = 'index.php';
include '../layouts/top.php';
require_once('../../models/Guest.php');
require_once('../../models/Room.php');
require_once('../../models/Reservation.php');
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Zimmer</th>
                    <th>Hauptgast</th>
                    <th>Startdatum</th>
                    <th>Enddatum</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $items = Reservation::getAll();
                        foreach($items as $key => $reservation) {
                    ?>
                <tr>
                    
                    <td><?= $reservation->getId() ?></td>
                    <td><?= Room::get($reservation->getRoom_id())->getName() ?></td>
                    <td><?= Guest::get($reservation->getGuest_id())->getName() ?></td>
                    <td><?= Reservation::formatDate($reservation->getDateFrom()) ?></td>
                    <td><?= Reservation::formatDate($reservation->getDateTo()) ?></td>
                    <td><a class="btn btn-info" href="view.php?id=<?= $reservation->getId() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id=<?= $reservation->getId() ?>"><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id=<?= $reservation->getId() ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                <?php
                        }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>