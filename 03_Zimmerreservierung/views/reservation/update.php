<?php
$title = "Zimmer bearbeiten";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = 'index.php';
include '../layouts/top.php';

require_once('../../models/Reservation.php');
require_once('../../models/Room.php');
require_once('../../models/Guest.php');

if (isset($_GET['id'])) {
    $reservation = Reservation::get($_GET['id']);
    $_SESSION['currentReservation'] = serialize($reservation);
}

if (isset($_POST['submit'])) {
    $reservation = unserialize($_SESSION['currentReservation']);
    $reservation->setRoom_id(isset($_POST['room_id']) ? $_POST['room_id'] : 0);
    $reservation->setGuest_id(isset($_POST['guest_id']) ? $_POST['guest_id'] : 0);
    $reservation->setDateFrom(isset($_POST['dateFrom']) ? $_POST['dateFrom'] : '');
    $reservation->setDateTo(isset($_POST['dateTo']) ? $_POST['dateTo'] : '');

    if ($reservation->save()) {
        header("Location: view.php?id=" . $reservation->getId());
        exit();
    }
}
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php" method="post">

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label for="room_id" class="control-label">Zimmer *</label>
                    <select name="room_id" class="form-control">
                        <option>--- Bitte auswählen ---</option>
                        <?php
                        foreach (Room::getAll() as $key => $room) {
                            $selected = '';
                            if ($room->getNr() == $reservation->getRoom_id()) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $room->getNr() . '" ' . $selected . '>' . $room->getName() . '</option>';
                        }
                        ?>
                    </select>
                    <?php if (!empty($reservation->getErrors()['room_id'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['room_id'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Hauptgast *</label>
                    <select name="guest_id" class="form-control">
                        <option>--- Bitte auswählen ---</option>
                        <?php
                        foreach (Guest::getAll() as $key => $guest) {
                            $selected = '';
                            if ($guest->getId() == $reservation->getGuest_id()) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $guest->getId() . '" ' . $selected . '>' . $guest->getName() . '</option>';
                        }
                        ?>
                    </select>
                    <?php if (!empty($reservation->getErrors()['guest_id'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['guest_id'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Startdatum *</label>
                    <input type="date" class="form-control" name="dateFrom" value="<?= $reservation->getDateFrom() ?>">
                    <?php if (!empty($reservation->getErrors()['dateFrom'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['dateFrom'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Enddatum *</label>
                    <input type="date" class="form-control" name="dateTo" value="<?= $reservation->getDateTo() ?>">
                    <?php if (!empty($reservation->getErrors()['dateTo'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['dateTo'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>