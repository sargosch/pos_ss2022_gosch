<?php
$title = "Zimmer anzeigen";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = 'index.php';
include '../layouts/top.php';

require_once('../../models/Guest.php');
require_once('../../models/Room.php');
require_once('../../models/Reservation.php');

$reservation = new Reservation();

if(isset($_GET['id'])) {
    $reservation = Reservation::get($_GET['id']);
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $reservation->getId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $reservation->getId() ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Reservierungsnummer</th>
                <td><?= $reservation->getId() ?></td>
            </tr>
            <tr>
                <th>Zimmer</th>
                <td><?= Room::get($reservation->getRoom_id())->getName() ?></td>
            </tr>
            <tr>
                <th>Hauptgast</th>
                <td><?= Guest::get($reservation->getGuest_id())->getName() ?></td>
            </tr>
            <tr>
                <th>Startdatum</th>
                <td><?= Reservation::formatDate($reservation->getDateFrom()) ?></td>
            </tr>
            <tr>
                <th>Enddatum</th>
                <td><?= Reservation::formatDate($reservation->getDateTo()) ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>