<?php
$title = "Zimmer löschen";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = 'index.php';
include '../layouts/top.php';

require_once('../../models/Reservation.php');

$reservation = Reservation::get($_GET['id']);

if(isset($_POST['delete'])) {
    Reservation::delete($_POST['id']);
    header("Location: index.php");
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?= $reservation->getId() ?>" method="post">
            <input type="hidden" name="id" value="<?= $reservation->getId() ?>"/>
            <p class="alert alert-error">Wollen Sie das Zimmer wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="delete" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>