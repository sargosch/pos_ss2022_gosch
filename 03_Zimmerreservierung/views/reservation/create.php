<?php
$title = "Zimmer erstellen";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = 'index.php';
include '../layouts/top.php';

require_once('../../models/Reservation.php');
require_once('../../models/Room.php');
require_once('../../models/Guest.php');

$reservation = new Reservation();

if (isset($_POST['submit'])) {
    $reservation->setRoom_id(isset($_POST['room_id']) ? $_POST['room_id'] : 0);
    $reservation->setGuest_id(isset($_POST['guest_id']) ? $_POST['guest_id'] : 0);
    $reservation->setDateFrom(isset($_POST['dateFrom']) ? $_POST['dateFrom'] : '');
    $reservation->setDateTo(isset($_POST['dateTo']) ? $_POST['dateTo'] : '');

    if ($reservation->save()) {
        header("Location: view.php?id=" . $reservation->getId());
        exit();
    }
}

?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <?php if (!empty($reservation->getErrors()['buchung'])) : ?>
        <div class="alert alert-danger"><?= $reservation->getErrors()['buchung'] ?></div>
    <?php endif; ?>

    <form class="form-horizontal" action="create.php" method="post">

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label for="room_id" class="control-label">Zimmer *</label>
                    <select id="room_id" name="room_id" class="form-control">
                        <option>--- Bitte auswählen ---</option>
                        <?php
                        foreach (Room::getAll() as $key => $room) {
                            $selected = '';
                            if ($room->getNr() == $reservation->getRoom_id()) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $room->getNr() . '" ' . $selected . '>' . $room->getName() . '</option>';
                        }
                        ?>
                    </select>
                    <?php if (!empty($reservation->getErrors()['room_id'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['room_id'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Hauptgast *</label>
                    <select name="guest_id" class="form-control">
                        <option>--- Bitte auswählen ---</option>
                        <?php
                        foreach (Guest::getAll() as $key => $guest) {
                            $selected = '';
                            if ($guest->getId() == $reservation->getGuest_id()) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $guest->getId() . '" ' . $selected . '>' . $guest->getName() . '</option>';
                        }
                        ?>
                    </select>
                    <?php if (!empty($reservation->getErrors()['guest_id'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['guest_id'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Startdatum *</label>
                    <input type="date" class="form-control" name="dateFrom" value="<?= $reservation->getDateFrom() ?>">
                    <?php if (!empty($reservation->getErrors()['dateFrom'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['dateFrom'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Enddatum *</label>
                    <input type="date" class="form-control" name="dateTo" value="<?= $reservation->getDateTo() ?>">
                    <?php if (!empty($reservation->getErrors()['dateTo'])) : ?>
                        <div class="help-block"><?= $reservation->getErrors()['dateTo'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
<!--<script>
$(function() {
    // AJAX-Anfrage, um verfügbare Reservierungsdaten für das Zimmer abzurufen
    $('#room_id').on('change', function() {
        var room_id = $(this).val();
        $.ajax({
            url: 'getAvailableDates.php',
            type: 'POST',
            data: { room_id: room_id },
            dataType: 'json',
            success: function(data) {
                $('#dateFrom, #dateTo').datepicker('destroy');
                $('#dateFrom, #dateTo').datepicker({
                    beforeShowDay: function(date) {
                        var dateStr = $.datepicker.formatDate('yy-mm-dd', date);
                        return [ data.indexOf(dateStr) == -1 ];
                    }
                });
            }
        });
    });
});
</script>-->
<?php
include '../layouts/bottom.php';
?>