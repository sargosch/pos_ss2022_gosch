<?php

require_once('../../models/Database.php');
// Verbindung zur Datenbank herstellen
$db = Database::connect();

// Guest-ID aus dem POST-Parameter holen
$room_id = $_POST['room_id'];

// Alle Reservierungen für das Zimmer abrufen
$stmt = $db->prepare('SELECT dateFrom, dateTo FROM reservation WHERE room_id = ?');
$stmt->execute([$room_id]);
$reservations = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Alle reservierten Tage sammeln
$reserved_days = array();
foreach ($reservations as $reservation) {
    $start_date = new DateTime($reservation['dateFrom']);
    $end_date = new DateTime($reservation['dateTo']);
    for ($i = $start_date; $i <= $end_date; $i->modify('+1 day')) {
        $reserved_days[] = $i->format('Y-m-d');
    }
}

Database::disconnect();

// Verfügbare Reservierungsdaten als JSON zurückgeben
echo json_encode($reserved_days);
