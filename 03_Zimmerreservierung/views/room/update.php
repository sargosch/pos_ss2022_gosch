<?php
$title = "Zimmer bearbeiten";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = 'index.php';
$pathToAllReservations = '../reservation/index.php';
include '../layouts/top.php';

require_once('../../models/Room.php');

$room = new Room();

if (isset($_GET['id'])) {
    $room = Room::get($_GET['id']);
    $_SESSION['currentRoom'] = serialize($room);
}


if(isset($_POST['submit'])) {
    $room = unserialize($_SESSION['currentRoom']);
    $room->setNr(isset($_POST['nr']) ? $_POST['nr'] : 0);
    $room->setName(isset($_POST['name']) ? $_POST['name'] : '');
    $room->setPersons(isset($_POST['persons']) ? $_POST['persons'] : 0);
    $room->setPrice(isset($_POST['price']) ? $_POST['price'] : 0.0);
    $room->setBalcony(isset($_POST['balcony']) ? true : false);

    if($room->save(true)) {
        header("Location: view.php?id=" . $room->getNr());
        exit();
    }
}
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Zimmernummer *</label>
                    <input type="text" class="form-control" name="nr" maxlength="4" value="<?= $room->getNr() ?>" readonly>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="64" value="<?= $room->getName() ?>">
                    <?php if(!empty($room->getErrors()['name'])): ?>
                    <div class="help-block"><?= $room->getErrors()['name'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Personen *</label>
                    <input type="number" class="form-control" name="persons" min="1" value="<?= $room->getPersons() ?>">
                    <?php if(!empty($room->getErrors()['persons'])): ?>
                    <div class="help-block"><?= $room->getErrors()['persons'] ?></div>
                    <?php endif; ?>   
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Preis *</label>
                    <input type="text" class="form-control" name="price" value="<?= $room->getPrice() ?>">
                    <?php if(!empty($room->getErrors()['price'])): ?>
                    <div class="help-block"><?= $room->getErrors()['price'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
                <div class="form-group required ">
                    <label class="control-label">Balkon *</label>
                    <input type="checkbox" class="form-control" name="balcony" <?= ($room->getBalcony() ? 'checked' : '') ?>>
                    <?php if(!empty($room->getErrors()['balcony'])): ?>
                    <div class="help-block"><?= $room->getErrors()['balcony'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
