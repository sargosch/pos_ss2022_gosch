<?php
$title = "Zimmerverwaltung";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = 'index.php';
$pathToAllReservations = '../reservation/index.php';
include '../layouts/top.php';
require_once('../../models/Room.php');
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Zimmernummer</th>
                    <th>Name</th>
                    <th>Personen</th>
                    <th>Preis</th>
                    <th>Balkon</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $items = Room::getAll();
                        foreach($items as $key => $room) {
                    ?>
                <tr>
                    
                    <td><?= $room->getNr() ?></td>
                    <td><?= $room->getName() ?></td>
                    <td><?= $room->getPersons() ?></td>
                    <td>€ <?= $room->getPrice() ?></td>
                    <td><?= ($room->getBalcony() ? 'Ja' : 'Nein') ?></td>
                    <td><a class="btn btn-info" href="view.php?id=<?= $room->getNr() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id=<?= $room->getNr() ?>"><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id=<?= $room->getNr() ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                <?php
                        }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>