<?php
$title = "Zimmer anzeigen";
$pathToAllGuests = '../guest/index.php';
$pathToAllRooms = 'index.php';
$pathToAllReservations = '../reservation/index.php';
include '../layouts/top.php';

require_once('../../models/Room.php');

$room = new Room();

if(isset($_GET['id'])) {
    $room = Room::get($_GET['id']);
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $room->getNr() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $room->getNr() ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Zimmernummer</th>
                <td><?= $room->getNr() ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?= $room->getName() ?></td>
            </tr>
            <tr>
                <th>Personen</th>
                <td><?= $room->getPersons() ?></td>
            </tr>
            <tr>
                <th>Preis</th>
                <td>€<?= $room->getPrice() ?></td>
            </tr>
            <tr>
                <th>Balkon</th>
                <td><?= ($room->getBalcony() ? 'Ja' : 'Nein') ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>