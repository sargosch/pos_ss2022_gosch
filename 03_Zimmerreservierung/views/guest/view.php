<?php
$title = "Zimmer anzeigen";
$pathToAllGuests = 'index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = '../reservation/index.php';
include '../layouts/top.php';

require_once('../../models/Guest.php');

$guest = new Guest();

if(isset($_GET['id'])) {
    $guest = Guest::get($_GET['id']);
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $guest->getId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $guest->getId() ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Zimmernummer</th>
                <td><?= $guest->getId() ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?= $guest->getName() ?></td>
            </tr>
            <tr>
                <th>Personen</th>
                <td><?= $guest->getEmail() ?></td>
            </tr>
            <tr>
                <th>Preis</th>
                <td><?= $guest->getAddress() ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>