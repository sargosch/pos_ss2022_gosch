<?php
$title = "Zimmer bearbeiten";
$pathToAllGuests = 'index.php';
$pathToAllRooms = '../room/index.php';
$pathToAllReservations = '../reservation/index.php';
include '../layouts/top.php';

require_once('../../models/Guest.php');

$guest = new Guest();

if (isset($_GET['id'])) {
    $guest = Guest::get($_GET['id']);
    $_SESSION['currentGuest'] = serialize($guest);
    
}

if(isset($_POST['submit'])) {
    $guest = unserialize($_SESSION['currentGuest']);
    $guest->setId(isset($_POST['id']) ? $_POST['id'] : 0);
    $guest->setName(isset($_POST['name']) ? $_POST['name'] : '');
    $guest->setEmail(isset($_POST['email']) ? $_POST['email'] : 0);
    $guest->setAddress(isset($_POST['address']) ? $_POST['address'] : 0.0);

    if($guest->save(true)) {
        header("Location: view.php?id=" . $guest->getId());
        exit();
    }
}
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">ID *</label>
                        <input type="text" class="form-control" name="id" maxlength="4" value="<?= $guest->getId() ?>">
                        <?php if(!empty($guest->getErrors()['id'])): ?>
                    <div class="help-block"><?= $guest->getErrors()['id'] ?></div>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="form-group required ">
                        <label class="control-label">Name *</label>
                        <input type="text" class="form-control" name="name" maxlength="64" value="<?= $guest->getName() ?>">
                        <?php if(!empty($guest->getErrors()['name'])): ?>
                    <div class="help-block"><?= $guest->getErrors()['name'] ?></div>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Email *</label>
                        <input type="text" class="form-control" name="email" min="1" value="<?= $guest->getEmail() ?>">
                        <?php if(!empty($guest->getErrors()['email'])): ?>
                    <div class="help-block"><?= $guest->getErrors()['email'] ?></div>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Preis *</label>
                        <input type="text" class="form-control" name="address" value="<?= $guest->getAddress() ?>">
                        <?php if(!empty($guest->getErrors()['address'])): ?>
                    <div class="help-block"><?= $guest->getErrors()['address'] ?></div>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
