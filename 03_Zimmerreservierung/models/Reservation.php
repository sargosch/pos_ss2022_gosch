<?php

require_once('DatabaseObject.php');
require_once('Room.php');
require_once('Guest.php');

class Reservation implements DatabaseObject
{

    private $id = 0;
    private $room_id = '';
    private $guest_id = '';
    private $dateFrom = '';
    private $dateTo = '';

    private $errors = [];

    public function __construct()
    {
    }

    public function validate()
    {
        return $this->isDateAvaiable($this->room_id, $this->dateFrom, $this->dateTo) &
            $this->validateNumber('Zimmernummer', 'room_id', $this->room_id) &
            $this->validateNumber('Gäste-ID', 'guest_id', $this->guest_id) &
            $this->validateDate('Startdatum', 'dateFrom', $this->dateFrom, $this->dateTo) &
            $this->validateDate('Enddatum', 'dateTo', $this->dateFrom, $this->dateTo);
    }

    private function validateNumber($label, $key, $value)
    {
        if ($value == 0 || $value == null) {
            $this->errors[$key] = "$label darf nicht null/leer sein!";
            return false;
        } else if ($key == 'room_id') {
            if (Room::get($value) == null) {
                $this->errors[$key] = "Ein Zimmer mit der Nr. $label ist nicht verfügbar!";
                return false;
            } else {
                return true;
            }
        } else if ($key == 'guest_id') {
            if (Guest::get($value) == null) {
                $this->errors[$key] = "Ein Gast mit der ID $label ist nicht verfügbar!";
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private function validateDate($label, $key, $dateFrom, $dateTo)
    {

        $today = date("Y-m-d");

        if ($key == 'dateFrom') {
            if ($dateFrom == null) {
                $this->errors[$key] = "$label darf nicht leer sein!";
                return false;
            } //else if($dateFrom < $today) {
            //$this->errors[$key] = "$label darf nicht in der Vergangenheit liegen!";
            //return false;
            //} 
            else if ($dateFrom > $dateTo) {
                $this->errors[$key] = "$label darf nicht nach dem Enddatum sein!";
                return false;
            } else {
                return true;
            }
        } else if ($key == 'dateTo') {
            if ($dateFrom == null) {
                $this->errors[$key] = "$label darf nicht leer sein!";
                return false;
            } else if ($dateTo < $dateFrom) {
                $this->errors[$key] = "$label darf nicht vor dem Startdatum sein!";
                return false;
            } else {
                return true;
            }
        } else if (($dateTo - $dateFrom) > 30) {
            $this->errors[$key] = "Die maximale Buchungsdauer beträgt 30 Tage!";
            return false;
        } else {
            return true;
        }
    }
/*($dateFrom <= $reservation->getDateFrom() 
            && ($dateTo >= $reservation->getDateFrom() && $dateTo <= $reservation->getDateTo() || $dateTo >= $reservation->getDateTo())) ||
            ($dateFrom >= $reservation->getDateFrom() && $dateTo <= $reservation->getDateTo()) ||
            ($dateFrom >= $reservation->getDateFrom() && ($dateTo <= $reservation->getDateTo())) ||
            ($dateFrom >= $reservation->getDateFrom() && ($dateTo >= $reservation->getDateTo()))
            
            ($dateFrom < $reservation->getDateFrom() && $dateTo > $reservation->getDateFrom()) &&
            ($dateFrom >= $reservation->getDateFrom() && ($dateTo <= $reservation->getDateTo() || $dateTo > $reservation->getDateTo())) &&
            ($dateFrom < $reservation->getDateTo() && $dateTo > $reservation->getDateTo())
    public function isDateAvaiable($room_id, $dateFrom, $dateTo)
    {
        $reservations = Reservation::getByRoom($room_id);
        $check = 0;

        $isAvailable = true;

        foreach($reservations as $key => $reservation) {
            $existingDateFrom = $reservation->getDateFrom();
            $existingDateTo = $reservation->getDateTo();

            $errorMessage = "Der von Ihnen gewünschte Buchungszeitraum kollidiert mit einer anderen Buchung! <br>
            -  Zimmer: " . Room::get($room_id)->getName() . " <br>
            -  Gebucht von: " . Reservation::formatDate($reservation->getDateFrom()) . " <br>
            -  Gebucht bis: " . Reservation::formatDate($reservation->getDateTo()) . " <br>
            Bitte wählen Sie einen anderen Zeitraum!";
            if(($dateFrom < $existingDateFrom && $dateTo <= $existingDateFrom) ||
            ($dateFrom >= $existingDateTo && $dateTo > $existingDateTo)) {
                $isAvailable = true;
            } else {
                $this->errors['buchung'] = $errorMessage;
                $isAvailable = false;
                return false;
            }

        }

        if($isAvailable) {
            return true;
        } else {
            return false;
        }
    }*/

    public function isDateAvaiable($room_id, $dateFrom, $dateTo) {
        $reservations = Reservation::getByRoom($room_id);
        $check = 0;

        foreach($reservations as $key => $reservation) {
            if(($dateFrom <= $reservation->getDateFrom() && ($dateTo >= $reservation->getDateFrom() && $dateTo <= $reservation->getDateTo() || $dateTo > $reservation->getDateTo())) ||
            ($dateFrom >= $reservation->getDateFrom() && $dateTo <= $reservation->getDateTo()) ||
            ($dateFrom >= $reservation->getDateFrom() && ($dateTo <= $reservation->getDateTo())) ||
            ($dateFrom >= $reservation->getDateFrom() && ($dateTo >= $reservation->getDateTo()))) {
                $this->errors['buchung'] = "Der von Ihnen gewünschte Buchungszeitraum kollidiert mit einer anderen Buchung! <br>
                  -  Zimmer: " . Room::get($room_id)->getName() . " <br>
                  -  Gebucht von: " . Reservation::formatDate($reservation->getDateFrom()) . " <br>
                  -  Gebucht bis: " . Reservation::formatDate($reservation->getDateTo()) . " <br>
                Bitte wählen Sie einen anderen Zeitraum!";
                $check += 1;
                return false;
            }
        }

        if($check == 0) {
            return true;
        } else {
            return false;
        }
    }


    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->create();
            }

            return true;
        }

        return false;
    }


    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();

        $sql = 'INSERT INTO `reservation`(`room_id`, `guest_id`, `dateFrom`, `dateTo`) VALUES (?,?,?,?)';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->room_id, $this->guest_id, $this->dateFrom, $this->dateTo));
        $lastId = $db->lastInsertId();

        Database::disconnect();

        return $lastId;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();

        $sql = 'UPDATE `reservation` SET room_id = ?, guest_id = ?, dateFrom = ?, dateTo = ? WHERE id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->room_id, $this->guest_id, $this->dateFrom, $this->dateTo, $this->id));

        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();

        $sql = 'SELECT * FROM `reservation` WHERE id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Reservation');

        Database::disconnect();

        //Wenn ein Datensatz gefunden wurde, also dieser nicht false ist, wird
        //der entsprechende Datensatz zurückgegeben - ansonsten wird null 
        //zurückgegeben!
        return $item !== false ? $item : null;
    }

    public static function getByRoom($id)
    {
        $db = Database::connect();

        $sql = 'SELECT * FROM `reservation` WHERE room_id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Reservation');

        Database::disconnect();

        return $items;
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {
        $db = Database::connect();

        $sql = 'SELECT * FROM `reservation` ORDER BY id ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Reservation');

        Database::disconnect();

        return $items;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();

        $sql = 'DELETE FROM `reservation` WHERE id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));

        Database::disconnect();
    }

    public static function getReservedDays($room_id)
    {
        $reservations = self::getByRoom($room_id);
        $reserved_days = array();

        foreach ($reservations as $reservation) {
            $start_date = new DateTime($reservation->getDateFrom());
            $end_date = new DateTime($reservation->getDateTo());

            for ($i = $start_date; $i <= $end_date; $i->modify('+1 day')) {
                $reserved_days[] = $i->format('Y-m-d');
            }
        }
        return $reserved_days;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of room_id
     */
    public function getRoom_id()
    {
        return $this->room_id;
    }

    /**
     * Set the value of room_id
     *
     * @return  self
     */
    public function setRoom_id($room_id)
    {
        $this->room_id = $room_id;

        return $this;
    }

    /**
     * Get the value of guest_id
     */
    public function getGuest_id()
    {
        return $this->guest_id;
    }

    /**
     * Set the value of guest_id
     *
     * @return  self
     */
    public function setGuest_id($guest_id)
    {
        $this->guest_id = $guest_id;

        return $this;
    }

    /**
     * Get the value of dateFrom
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set the value of dateFrom
     *
     * @return  self
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get the value of dateTo
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set the value of dateTo
     *
     * @return  self
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get the value of errors
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    public static function formatDate($date)
    {
        $dateFromated = date_create($date);
        return date_format($dateFromated, 'd.m.Y');
    }
}
