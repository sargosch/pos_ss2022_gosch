<?php

require_once('DatabaseObject.php');

class Room implements DatabaseObject {

    private $nr = 0;
    private $name = '';
    private $persons = '';
    private $price = '';
    private $balcony = '';

    private $errors = [];

    public function __construct()
    {
        
    }

    public function validate($update) {
        return $this->validateNumber($update, 'Nr', 'nr', $this->nr, 100) &
        $this->validateText('Name', 'name', $this->name, 50) & 
        $this->validateNumber($update, 'Persons', 'persons', $this->persons, 4) &
        $this->validatePrice('Price', 'price', $this->price, 500.00);
    }

    private function validateText($label, $key, $value, $maxLenght) {
        if(strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein!";
            return false;
        } else if(strlen($value) >= $maxLenght) {
            $this->errors[$key] = "$label zu lang (max. $maxLenght Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    private function validatePrice($label, $key, $value, $maxLenght) {
        if($value == 0.0 || $value == null) {
            $this->errors[$key] = "$label darf nicht null/leer sein!";
            return false;
        } else if($value >= $maxLenght) {
            $this->errors[$key] = "$label zu hoch (max. $maxLenght €)";
            return false;
        } else {
            return true;
        }
    }

    private function validateNumber($update, $label, $key, $value, $maxLenght) {
        if($key == 'nr') {
            if(!$update && Room::get($value) != null) {
                $this->errors[$key] = "$label bereits vorhanden!";
                return false;
            }
        }
        if($value == 0 || $value == null) {
            $this->errors[$key] = "$label darf nicht null/leer sein!";
            return false;
        } else if($value > $maxLenght) {
            $this->errors[$key] = "$label zu groß (max. $maxLenght )";
            return false;
        } else {
            return true;
        }
    }

    public function save($update) {
        if($this->validate($update)) {
            if(Room::get($this->nr) != null) {
                $this->update();

            } else {
                $this->create();
                
            }

            return true;
        }

        return false;
    }


    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create(){
        $db = Database::connect();

        $sql = 'INSERT INTO `room`(`nr`, `name`, `persons`, `price`, `balcony`) VALUES (?,?,?,?,?)';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->nr, $this->name, $this->persons, $this->price, $this->balcony));
        $lastId = $db->lastInsertId();

        Database::disconnect();

        return $lastId;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update(){
        $db = Database::connect();

        $sql = 'UPDATE `room` SET name = ?, persons = ?, price = ?, balcony = ? WHERE nr = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->persons, $this->price, $this->balcony, $this->nr));

        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id){
        $db = Database::connect();

        $sql = 'SELECT * FROM `room` WHERE nr = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Room');

        Database::disconnect();

        //Wenn ein Datensatz gefunden wurde, also dieser nicht false ist, wird
        //der entsprechende Datensatz zurückgegeben - ansonsten wird null 
        //zurückgegeben!
        return $item !== false ? $item : null;
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll(){
        $db = Database::connect();

        $sql = 'SELECT * FROM `room` ORDER BY nr ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Room');

        Database::disconnect();

        return $items;
        echo $items;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id){
        $db = Database::connect();

        $sql = 'DELETE FROM `room` WHERE nr = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));

        Database::disconnect();
    }

    

    /**
     * Get the value of nr
     */ 
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * Set the value of nr
     *
     * @return  self
     */ 
    public function setNr($nr)
    {
        $this->nr = $nr;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of persons
     */ 
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * Set the value of persons
     *
     * @return  self
     */ 
    public function setPersons($persons)
    {
        $this->persons = $persons;

        return $this;
    }

    /**
     * Get the value of price
     */ 
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */ 
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of balcony
     */ 
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * Set the value of balcony
     *
     * @return  self
     */ 
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;

        return $this;
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }
}