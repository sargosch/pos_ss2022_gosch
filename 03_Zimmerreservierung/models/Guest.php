<?php

require_once('DatabaseObject.php');

class Guest implements DatabaseObject {

    private $id = 0;
    private $name = '';
    private $email = '';
    private $address = '';

    private $errors = [];

    public function __construct()
    {
        
    }

    public function validate() {
        return $this->validateHelper('Name', 'name', $this->name, 32) & 
        $this->validateHelper('Email', 'email', $this->email, 255) &
        $this->validateHelper('Address', 'address', $this->address, 255);
    }

    private function validateHelper($label, $key, $value, $maxLenght) {
        if(strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein!";
            return false;
        } else if(strlen($value) >= $maxLenght) {
            $this->errors[$key] = "$label zu lang (max. $maxLenght Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    public function save() {
        if($this->validate()) {
            
            if($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }


    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create(){
        $db = Database::connect();

        $sql = 'INSERT INTO `guest`(`name`, `email`, `address`) VALUES (?,?,?)';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->email, $this->address));
        $lastId = $db->lastInsertId();

        Database::disconnect();

        return $lastId;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update(){
        $db = Database::connect();

        $sql = 'UPDATE `guest` SET `name`=?,`email`=?,`address`=? WHERE `id`=?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->email, $this->address, $this->id));

        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id){
        $db = Database::connect();

        $sql = 'SELECT * FROM `guest` WHERE id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Guest');

        Database::disconnect();

        //Wenn ein Datensatz gefunden wurde, also dieser nicht false ist, wird
        //der entsprechende Datensatz zurückgegeben - ansonsten wird null 
        //zurückgegeben!
        return $item !== false ? $item : null;
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll(){
        $db = Database::connect();

        $sql = 'SELECT * FROM `guest` ORDER BY id ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Guest');

        Database::disconnect();

        return $items;
        echo $items;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id){
        $db = Database::connect();

        $sql = 'DELETE FROM `guest` WHERE id = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));

        Database::disconnect();
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of address
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */ 
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }
}