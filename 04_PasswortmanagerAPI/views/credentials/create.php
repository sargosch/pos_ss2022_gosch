<div class="container">
    <div class="row">
        <h2>Zugangsdaten erstellen</h2>
    </div>

    <form class="form-horizontal" action="index.php?r=credentials/create" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('name') ? 'has-error' : '' ?>">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="32"
                           value="<?= $model->getName() ?>">
                    <?php if(!empty($model->hasError('name'))): ?>
                        <div class="help-block"><?= $model->getError('name') ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('domain') ? 'has-error' : '' ?>">
                    <label class="control-label">Domäne *</label>
                    <input type="text" class="form-control" name="domain" maxlength="128"
                           value="<?= $model->getDomain() ?>">
                    <?php if(!empty($model->hasError('domain'))): ?>
                        <div class="help-block"><?= $model->getError('domain') ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('cms_username') ? 'has-error' : '' ?>">
                    <label class="control-label">CMS-Benutzername *</label>
                    <input type="text" class="form-control" name="cms_username" maxlength="64"
                           value="<?= $model->getCms_username() ?>">
                    <?php if(!empty($model->hasError('cms_username'))): ?>
                        <div class="help-block"><?= $model->getError('cms_username') ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('cms_password') ? 'has-error' : '' ?>">
                    <label class="control-label">CMS-Passwort *</label>
                    <input type="text" class="form-control" name="cms_password" maxlength="64"
                           value="<?= $model->getCms_password() ?>">
                    <?php if(!empty($model->hasError('cms_password'))): ?>
                        <div class="help-block"><?= $model->getError('cms_password') ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php?r=credentials/index">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
