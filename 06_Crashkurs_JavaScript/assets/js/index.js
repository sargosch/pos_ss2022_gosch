//Handler wird ausgeführt, sobald DOM vollständig geladen wurde.
document.addEventListener('DOMContentLoaded', function () {

  //Koordinaten von Imst
  //Verbesserung: Hole Koordinaten von der Geolocation-API
  let long = 10.7455712;
  let lat = 47.2242934;
  let city = "Imst";

  //loadCurrentTemperature(city);

  //https://history.openweathermap.org/data/2.5/history/city?lat=47.2242934&lon=10.7455712&type=hour&start=1679817600&end=1683043199&appid=4d80f8b5158e038ee9ae9cff8d772b3f

  var searchButton = document.getElementById("searchButton");
  searchButton.onclick = function () {
    var cityInputEl = document.getElementById("city");
    loadCurrentTemperature(cityInputEl.value);
    loadForecastTemperature(cityInputEl.value);
  }

  var filterTempToday = document.getElementById("tempToday");
  filterTempToday.onclick = function() {
    var cityInputEl = document.getElementById("city");
    loadCurrentTemperature(cityInputEl.value);
  }

  var filterTemp7Days = document.getElementById("temp7Days");
  filterTemp7Days.onclick = function() {
    var cityInputEl = document.getElementById("city");
    getFilteredTempData(cityInputEl.value, 7);
  }

  var filterTemp40Days = document.getElementById("temp40Days");
  filterTemp40Days.onclick = function() {
    var cityInputEl = document.getElementById("city");
    getFilteredTempData(cityInputEl.value, 40);
  }

  function getFilteredTempData(city, filter) {
    
    var xhr1 = new XMLHttpRequest();
    xhr1.open("GET", "https://api.openweathermap.org/data/2.5/forecast?q="+city+",AT&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f", true);
    xhr1.responseType = 'json';
    xhr1.onload = function () {
      var datalist = xhr1.response.list;
      var sumTemp = 0;
      var sumFeelsLike = 0;
      var counter = 0;

      for (const element of datalist) {
        if(counter < filter) {
          sumTemp += element.main.temp;
          sumFeelsLike += element.main.feels_like;
        }
        ++counter;
      }
      var tempEl = document.getElementById("temp");
      var feelsLikeEl = document.getElementById("feelsLike");

      tempEl.textContent = (sumTemp/filter).toFixed(2) + "°C";
      feelsLikeEl.textContent = (sumFeelsLike/filter).toFixed(2) + "°C";
    }
    xhr1.send();
  }

  var filterHumiToday = document.getElementById("humiToday");
  filterHumiToday.onclick = function() {
    var cityInputEl = document.getElementById("city");
    loadCurrentTemperature(cityInputEl.value);
  }

  var filterHumi7Days = document.getElementById("humi7Days");
  filterHumi7Days.onclick = function() {
    var cityInputEl = document.getElementById("city");
    getFilteredHumiData(cityInputEl.value, 7);
  }

  var filterHumi40Days = document.getElementById("humi40Days");
  filterHumi40Days.onclick = function() {
    var cityInputEl = document.getElementById("city");
    getFilteredHumiData(cityInputEl.value, 40);
  }

  function getFilteredHumiData(city, filter) {
    
    var xhr1 = new XMLHttpRequest();
    xhr1.open("GET", "https://api.openweathermap.org/data/2.5/forecast?q="+city+",AT&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f", true);
    xhr1.responseType = 'json';
    xhr1.onload = function () {
      var datalist = xhr1.response.list;
      var sumHumi = 0;
      var counter = 0;

      for (const element of datalist) {
        if(counter < filter) {
          sumHumi += element.main.humidity;
        }
        ++counter;
      }
      var humiEl = document.getElementById("humidity");

      humiEl.textContent = (sumHumi/filter).toFixed(2) + "%";
    }
    xhr1.send();
  }


  function loadCurrentTemperature(city) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.openweathermap.org/data/2.5/weather?q=" + city + ",AT&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f", true);
    xhr.responseType = 'json';
    xhr.onload = function () {
      var data = xhr.response;
      console.log(data);
      changeCurrentTemperature(data);
    }
    xhr.send();
  }

  function changeCurrentTemperature(data) {

    var temp = data.main.temp;
    var feelsLike = data.main.feels_like;
    var humidity = data.main.humidity;

    var tempEl = document.getElementById("temp");
    var feelsLikeEl = document.getElementById("feelsLike");
    var humidityEl = document.getElementById("humidity");

    tempEl.textContent = temp + "°C";
    feelsLikeEl.textContent = feelsLike + "°C";
    humidityEl.textContent = humidity + "%";

  }

  function loadForecastTemperature(city) {

    var xhr1 = new XMLHttpRequest();
    xhr1.open("GET", "https://api.openweathermap.org/data/2.5/forecast?q=" + city + ",AT&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f", true);
    xhr1.responseType = 'json';
    xhr1.onload = function () {
      var datalist = xhr1.response;
      loadChart(datalist);
    }
    xhr1.send();
  }


  //loadChart([]);

  function loadChart(data) {

    var datalist = data.list;
    var temps = [];
    var date = ["heute", "morgen", "übermorgen"];
    var counter = 1;
    for (const element of datalist) {
      temps.push(element.main.temp);
      if(counter >= 3){
        date.push("+"+counter);
      }
      ++counter;
    }
    

    //Wenn bereits ein Chart existiert, wird dieses gelöscht.
    let oldChart = Chart.getChart("#lineChart");
    if (oldChart != undefined) {
      oldChart.destroy();
    }

    new Chart(document.querySelector('#lineChart'), {
      type: 'line',
      data: {
        labels: date,
        datasets: [{
          label: 'Temp. der nächsten 7 Tage',
          data: temps,
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0.1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }



});