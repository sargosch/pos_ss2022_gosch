# Demo JavaSript, DOM, AJAX, ChartJS

Dient der Testvorbereitung

## Verwendete Ressourcen:

OpenWeatherMap-API, devdocs.io

## Doku
https://openweathermap.org/api

Zum selbst Ausprobieren sollte eine eigene AppID erzeugt werden. Diese dient nur Demozwecken und wird nach kurzer Zeit gelöscht.

## Endpoint zum Abfragen der aktuellen Wetterdaten nach Geokoordinaten oder Städtenamen:
https://api.openweathermap.org/data/2.5/weather?lat=47.2242934&lon=10.7455712&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f

https://api.openweathermap.org/data/2.5/weather?q=Imst,AT&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f

## Endpoint zum Abfragen der der Wettervorhersage nach Geokoordinaten:
https://api.openweathermap.org/data/2.5/forecast/daily?lat=47.2242934&lon=0.7455712&cnt=7&units=metric&lang=de&appid=5e8a64ec0e418c20aafdf707c149e3ee

https://api.openweathermap.org/data/2.5/forecast/daily?q=Imst,AT&cnt=7&units=metric&lang=de&appid=4d80f8b5158e038ee9ae9cff8d772b3f

