<?php

session_start();

require_once("models/User.php");

if (!empty($_POST)) {
    if(isset($_POST['login'])) {
        User::login(isset($_POST['email']) ? $_POST['email'] : '', isset($_POST['password']) ? $_POST['password'] : '');
    } elseif(isset($_POST['logout'])) {
        User::logout();
    }
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Awesome CMS</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<?php
include "views/helper/navbar.php";
?>
<div class="jumbotron">
    <div class="container">
        <h1>Hello Awesome CMS!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
</div>
<div class="container">
    <!-- Example row of columns -->
    <?php
        require_once('models/Article.php');
        $artikel1 = Article::get(1);
        $artikel2 = Article::get(4);
        $artikel3 = Article::get(7);
    ?>

    <div class="row">
        <div class="col-md-4">
            <h2><?php echo htmlspecialchars($artikel1->title)?></h2>
            <p><?php echo htmlspecialchars($artikel1->content)?></p>
            <p><a class="btn btn-default" href="views/article/view.php?id=<?php echo $artikel1->id?>" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2><?php echo htmlspecialchars($artikel2->title)?></h2>
            <p><?php echo htmlspecialchars($artikel2->content)?></p>
            <p><a class="btn btn-default" href="views/article/view.php?id=<?php echo $artikel2->id?>" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2><?php echo htmlspecialchars($artikel3->title)?></h2>
            <p><?php echo htmlspecialchars($artikel3->content)?></p>
            <p><a class="btn btn-default" href="views/article/view.php?id=<?php echo $artikel3->id?>" role="button">View details &raquo;</a></p>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; 2017 Company, Inc.</p>
    </footer>
</div> <!-- /container -->

</body>
</html>