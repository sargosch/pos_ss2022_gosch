<?php

require_once("DatabaseObject.php");
require_once("User.php");

class Article implements DatabaseObject
{
    private $id;
    private $owner;
    private $title;
    private $content;
    private $releasedate;

    private $errors;

    public function __construct($id = 0, $owner = null, $title = '', $content = '', $releasedate = '')
    {
        $this->id = $id;
        $this->owner = $owner;
        $this->title = $title;
        $this->content = $content;
        $this->releasedate = $releasedate;
        $this->errors = [];
    }

    public function validate()
    {
        return $this->validateOwner() & $this->validateTitle() & $this->validateContent() & $this->validateReleasedate();
    }

    /**
     * create or update an object
     * @return boolean true on success
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO article (owner, title, content, releasedate) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->owner->id, $this->title, $this->content, $this->releasedate));
        $this->id = $db->lastInsertId();
        Database::disconnect();
        return $this->id;
    }

    /**
     * Saves the object to the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE article set owner = ?, title = ?, content = ?, releasedate = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->owner->id, $this->title, $this->content, $this->releasedate, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT *, article.id AS a_id, user.id AS u_id FROM article JOIN user on article.owner = user.id where article.id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $d = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($d == null) {
            return null;
        } else {
            $owner = new User($d['u_id'], $d['name'], $d['email'], $d['password']);
            $a = new Article($d['a_id'], $owner, $d['title'], $d['content'], $d['releasedate']);
            return $a;
        }
    }

    /**
     * Get an array of objects from database
     * @return array array of objects
     */
    public static function getAll()
    {
        $articles = [];
        $db = Database::connect();
        $sql = 'SELECT *, article.id AS a_id, user.id AS u_id FROM article JOIN user on article.owner = user.id ORDER BY article.title ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();

        foreach ($data as $d) {
            $owner = new User($d['u_id'], $d['name'], $d['email'], $d['password']);
            $a = new Article($d['a_id'], $owner, $d['title'], $d['content'], $d['releasedate']);
            $articles[] = $a;
        }
        return $articles;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM article WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    private function validateOwner()
    {
        if ($this->owner == null || $this->owner->id <= 0) {
            $this->errors['owner'] = "Besitzer ist ungültig";
            return false;
        } else {
            return true;
        }
    }

    private function validateTitle()
    {
        if (strlen($this->title) == 0) {
            $this->errors['title'] = "Titel darf nicht leer sein";
            return false;
        } else if (strlen($this->title) > 45) {
            $this->errors['title'] = "Titel zu lang (max. 45 Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    private function validateContent()
    {
        if (strlen($this->content) == 0) {
            $this->errors['content'] = "Inhalt darf nicht leer sein";
            return false;
        } else {
            return true;
        }
    }

    private function validateReleasedate()
    {
        $d = DateTime::createFromFormat('Y-m-d', $this->releasedate);
        if ($d && $d->format('Y-m-d') == $this->releasedate) {
            return true;
        } else {
            $this->errors['releasedate'] = "Ungültiges Datum";
            return false;
        }
    }

    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $name
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}