<?php

require_once("DatabaseObject.php");

class User implements DatabaseObject
{
    private $id;
    private $name;
    private $email;
    private $password;

    private $errors;

    public function __construct($id = 0, $name = '', $email = '', $password = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->errors = [];
    }

    public function validate()
    {
        return $this->validateName() & $this->validateEmail() & $this->validatePassword();
    }

    /**
     * create or update an object
     * @return boolean true on success
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO user (name, email, password) values(?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->email, $this->password));
        $this->id = $db->lastInsertId();
        Database::disconnect();
        return $this->id;
    }

    /**
     * Saves the object to the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE user set name = ?, email = ?, password = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->email, $this->password, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM user where id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $d = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($d == null) {
            return null;
        } else {
            return new User($id, $d['name'], $d['email'], $d['password']);
        }
    }

    /**
     * Get an array of objects from database
     * @return array array of objects
     */
    public static function getAll()
    {
        $users = [];
        $db = Database::connect();
        $sql = 'SELECT * FROM user ORDER BY name ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $d) {
            $users[] = new User($d['id'], $d['name'], $d['email'], $d['password']);
        }
        return $users;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM user WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    private function validateName()
    {
        if (strlen($this->name) == 0) {
            $this->errors['name'] = "Name darf nicht leer sein";
            return false;
        } else if (strlen($this->name) > 45) {
            $this->errors['name'] = "Name zu lang (max. 45 Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    private function validateEmail()
    {
        if (strlen($this->email) == 0) {
            $this->errors['email'] = "E-Mail darf nicht leer sein";
            return false;
        } else if (strlen($this->email) > 45) {
            $this->errors['email'] = "E-Mail zu lang (max. 45 Zeichen)";
            return false;
        } else if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = "Keine gültige E-Mail angegeben";
            return false;
        } else {
            return true;
        }
    }

    private function validatePassword()
    {
        if (strlen($this->password) == 0) {
            $this->errors['password'] = "Passwort darf nicht leer sein";
            return false;
        } else if (strlen($this->password) > 45) {
            $this->errors['password'] = "Passwort zu lang (max. 45 Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    /**
     * try to login
     * @return bool
     */
    public static function login($email, $password)
    {
        $db = Database::connect();
        $sql = "SELECT id FROM user where email = ? AND password = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($email, $password));
        $d = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($d == null) {
            unset($_SESSION['id']);
            return false;
        } else {
            $_SESSION['id'] = $d['id'];
            return true;
        }
    }

    public static function logout()
    {
        unset($_SESSION['id']);
        session_destroy();
    }

    public static function isLoggedIn()
    {
        return isset($_SESSION['id']) && $_SESSION['id'] > 0;
    }

    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $name
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}