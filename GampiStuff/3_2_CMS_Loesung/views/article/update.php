<?php

session_start();

require_once("../../models/User.php");
require_once("../../models/Article.php");

if (!User::isLoggedIn()) {
    header('Location: /php32/index.php');
    exit();
}

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else {
    $article = Article::get($_GET['id']);
}

if ($article == null) {
    http_response_code(404);
    exit();
}

if (!empty($_POST)) {
    $article->owner = User::get(isset($_POST['owner']) ? $_POST['owner'] : 0);
    $article->title = isset($_POST['title']) ? $_POST['title'] : '';
    $article->content = isset($_POST['content']) ? $_POST['content'] : '';
    $article->releasedate = isset($_POST['releasedate']) ? $_POST['releasedate'] : '';

    if($article->validate()) {
        $article->save();
        header('Location: view.php?id=' . $article->id);
        exit();
    }
}

?>
<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <div class="row">
        <h2>Beitrag bearbeiten</h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $article->id ?>" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($article->errors['title']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45" value="<?= $article->title ?>">
                    <?php if (!empty($article->errors['title'])): ?>
                        <div class="help-block"><?= $article->errors['title'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required <?php echo !empty($article->errors['releasedate']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate" value="<?= $article->releasedate ?>">
                    <?php if (!empty($article->errors['releasedate'])): ?>
                        <div class="help-block"><?= $article->errors['releasedate'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required <?php echo !empty($article->errors['owner']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Besitzer *</label>
                    <select class="form-control" name="owner">
                        <option value="0">-Besitzer auswählen-</option>
                        <?php
                            foreach(User::getAll() as $user) {
                                echo '<option value="' . $user->id . '" ' . ($user->id == $article->owner->id ? 'selected' : '') . '>' . $user->name . '</option>';
                            }
                        ?>
                    </select>
                    <?php if (!empty($article->errors['owner'])): ?>
                        <div class="help-block"><?= $article->errors['owner'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required <?php echo !empty($article->errors['content']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10"><?= $article->content ?></textarea>
                    <?php if (!empty($article->errors['content'])): ?>
                        <div class="help-block"><?= $article->errors['content'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>