<?php

session_start();

require_once("../../models/User.php");
require_once("../../models/Article.php");

if (!User::isLoggedIn()) {
    header('Location: /php32/index.php');
    exit();
}

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else {
    $article = Article::get($_GET['id']);
}

if ($article == null) {
    http_response_code(404);
    exit();
}

?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <h2>Beitrag anzeigen</h2>

    <p>
        <a class="btn btn-primary" href="update.php?id=<?= $article->id ?>">Aktualisieren</a>
        <a class="btn btn-danger" href="delete.php?id=<?= $article->id ?>">Löschen</a>
        <a class="btn btn-default" href="index.php">Zurück</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Titel</th>
            <td><?= $article->title ?></td>
        </tr>
        <tr>
            <th>Freigabedatum</th>
            <td><?= $article->releasedate ?></td>
        </tr>
        <tr>
            <th>Besitzer</th>
            <td><?= $article->owner->name ?></td>
        </tr>
        <tr>
            <th>Inhalt</th>
            <td><?= $article->content ?></td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
</body>
</html>