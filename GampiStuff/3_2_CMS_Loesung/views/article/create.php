<?php

session_start();

require_once("../../models/User.php");
require_once("../../models/Article.php");

if (!User::isLoggedIn()) {
    header('Location: /php32/index.php');
    exit();
}

$article = new Article();
$article->owner = User::get($_SESSION['id']);   // use id of current logged in user

if (!empty($_POST)) {

    $article->title = isset($_POST['title']) ? $_POST['title'] : '';
    $article->content = isset($_POST['content']) ? $_POST['content'] : '';
    $article->releasedate = isset($_POST['releasedate']) ? $_POST['releasedate'] : '';

    if ($article->validate()) {
        $article->save();
        header("Location: view.php?id=" . $article->id);
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <div class="row">
        <h2>Beitrag erstellen</h2>
    </div>

    <form class="form-horizontal" action="create.php" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($article->errors['title']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45" value="<?= $article->title ?>">
                    <?php if (!empty($article->errors['title'])): ?>
                        <div class="help-block"><?= $article->errors['title'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required <?php echo !empty($article->errors['releasedate']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate" value="<?= $article->releasedate ?>">
                    <?php if (!empty($article->errors['releasedate'])): ?>
                        <div class="help-block"><?= $article->errors['releasedate'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required <?php echo !empty($article->errors['owner']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Besitzer *</label>
                    <input type="text" class="form-control" value="<?= $article->owner->name ?>" readonly >
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required <?php echo !empty($article->errors['content']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10"><?= $article->content ?></textarea>
                    <?php if (!empty($article->errors['content'])): ?>
                        <div class="help-block"><?= $article->errors['content'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>