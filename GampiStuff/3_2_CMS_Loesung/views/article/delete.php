<?php

session_start();

require_once("../../models/User.php");
require_once("../../models/Article.php");

if (!User::isLoggedIn()) {
    header('Location: /php32/index.php');
    exit();
}

if (isset($_POST['id'])) {
    Article::delete($_POST['id']);
    header("Location: index.php");
    exit();
} else {
    $article = Article::get(isset($_GET['id']) ? $_GET['id'] : 0);
}

if ($article == null) {
    http_response_code(404);
    exit();
}

?>
<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <h2>Beitrag löschen</h2>

    <form class="form-horizontal" action="delete.php?id=<?= $article->id ?>" method="post">
        <input type="hidden" name="id" value="<?= $article->id ?>"/>
        <p class="alert alert-error">Wollen Sie den Beitrag <?= $article->title ?> wirklich löschen?</p>
        <div class="form-actions">
            <button type="submit" class="btn btn-danger">Löschen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>