<?php

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

//$path = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);


function getRelativePath($basepath, $path) {
    $separator = '/';
    $basepath = array_slice(explode($separator, rtrim($basepath,$separator)),1);
    $path = array_slice(explode($separator, rtrim($path,$separator)),1);
    return $separator.implode($separator, array_slice($path, count($basepath)));
} 


$path = "/PHP/HTML-PHP-Demos/3_2_CMS_Loesung/";
$beitraegepfad = 'views/article';

            ?>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $path?>">Awesome CMS</a>
            <h1>
                
          
               
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php
            if (User::isLoggedIn()) {
            ?>
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $path.'views/article'?>">Beiträge</a></li>
                    <li><a href="#">Benutzer</a></li>
                </ul>
                <form class="navbar-form navbar-right" action="<?php echo $path?>" method="post">
                    <button type="submit" name="logout" class="btn btn-warning">Abmelden</button>
                </form>
            <?php
            } else {
            ?>
                <form class="navbar-form navbar-right" action="index.php" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="E-Mail" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control" name="password">
                    </div>
                    <button type="submit" name="login" class="btn btn-success">Anmelden</button>
                </form>

                <?php
            }
            ?>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>