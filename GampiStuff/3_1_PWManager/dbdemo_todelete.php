<?php
//Demoscript für den DB-Zugriff

require_once('models/Credential.class.php');

$dsn = 'mysql:dbname=pwmanager;host=127.0.0.1';
$user = 'root';
$passwort = '';
try{
    $dbconn = new PDO($dsn, $user, $passwort);
    //Setze die Fehlerbehandlung auf Exception Handling
    $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "DB-Verbindung erfolgreich!";
    $id = 5;
    $query = 'SELECT * FROM t_credentials WHERE id = :id';

    //Liefert ein PDOStatement-Objekt zurück
    $statement = $dbconn->prepare($query);


    //Databinding der Variablen
    $binding = $statement->bindValue(':id', $id, PDO::PARAM_INT);

    //Führe das Prepared Statement aus
    $statement->execute();

    $credentials = array();
    while ($ergebnis = $statement->fetchObject('Credential')) {
        //Hole eine Zeile als Objekt vom Typ stdClass

        //Objekt-Relationales Mapping hin zu Credential-Objekten
        // $credential = new Credential($ergebnis->name, $ergebnis->domain, $ergebnis->username, $ergebnis->password);
        // $credential->setId($ergebnis->id);

        //Sammle alle Objekte in einem Array
        $credentials[] = $ergebnis;
    }
    var_dump($credentials);
}
catch(Exception $e)
{
    echo $e->getMessage();
}



?>