<?php

require_once('DatabaseObject.php');

class Credential implements DatabaseObject
{
    private $id = 0;
    private $name = '';
    private $domain = '';
    private $username = '';
    private $password = '';

    private $errors = array();

    public function __construct()
    {
        
    }

    // public function __construct($name, $domain, $username, $password)
    // {
    //     $this->setName($name);
    //     $this->setDomain($domain);
    //     $this->setUsername($username);
    //     $this->setPassword($password);
    // }


    public function validate()
    {
        return self::validateHelper('Name', 'name', $this->name, 32) &
            self::validateHelper('Domäne', 'domain', $this->domain, 32) &
            self::validateHelper('Username', 'cms_username', $this->username, 32) &
            self::validateHelper('Passwort', 'cms_password', $this->password, 32);
    }


    private function validateHelper($label, $key, $value, $maxlength)
    {
        if(strlen($value) == 0)
        {
            $this->errors[$key] = "$label darf nicht leer sein.";
            return false;
        }
        elseif(strlen($value) > $maxlength)
        {
            $this->errors[$key] = "$label zu lang. Bitte nur $maxlength Zeichen.";
            return false;
        }
        else
        {
            return true;
        }

    }

    public function save()
    {
        if ($this->validate())
        {
            if ($this->id != null && $this->id > 0)
            {
                //Update
                $this->update();
            }
            else{
                //Create
                $this->create();
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        $db = Database::connect();
        
        $sql = "INSERT INTO t_credentials (name, domain, username, password) VALUES (?,?,?,?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1,$this->name,PDO::PARAM_STR);
        $stmt->bindParam(2,$this->domain,PDO::PARAM_STR);
        $stmt->bindParam(3,$this->username,PDO::PARAM_STR);
        $stmt->bindParam(4,$this->password,PDO::PARAM_STR);
        $stmt->execute();
        $lastid = $db->lastInsertId();
        $this->setId($lastid);
        Database::disconnect();
        return $lastid;
    }

    public function update()
    {
        $db = Database::connect();
        //$db = new PDO();
        $sql = "UPDATE t_credentials SET name = ?, domain = ?, username = ?, password = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1,$this->name,PDO::PARAM_STR);
        $stmt->bindParam(2,$this->domain,PDO::PARAM_STR);
        $stmt->bindParam(3,$this->username,PDO::PARAM_STR);
        $stmt->bindParam(4,$this->password,PDO::PARAM_STR);
        $stmt->bindParam(5,$this->id, PDO::PARAM_INT);
        $stmt->execute();
        Database::disconnect();
    }
  

    public static function delete($id)
    {
        $db = Database::connect(); 
        $sql = "DELETE FROM t_credentials WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(1,$id, PDO::PARAM_INT);
        $stmt->execute();
        Database::disconnect();
    }

    
    public static function get($id)
    {
        $db = Database::connect();
        // $db = new PDO('','','');
        $sql = "SELECT * FROM t_credentials WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(1,$id, PDO::PARAM_INT);
        $stmt->execute();
        $item = $stmt->fetchObject('Credential');
        Database::disconnect();
        return $item !== false ? $item : false;
    }


    public static function getAll()
    {
        $db = Database::connect();
        // $db = new PDO('','','');
        $sql = "SELECT * FROM t_credentials ORDER BY name, domain";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Credential');
        // var_dump($items);
        Database::disconnect();
        return $items !== false ? $items : false;
    }





    /**
     * Set the value of name
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Set the value of domain
     */
    public function setDomain($domain): self
    {
        $this->domain = $domain;

        return $this;
    }


    /**
     * Set the value of username
     */
    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }


    /**
     * Set the value of password
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Get the value of domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }
}
