<?php

class GradeEintrag
{
    private $studiname;
    private $fach;
    private $email;
    private $note;
    private $datum;
    private $fehlerliste = array();

    public function __construct($sname,$fach,$mail,$note,$date)
    {
        // Validieren, indem die richtigen Setter-Methoden verwendet werden
        $this->setStudiname($sname);
        //Todo: Restliche Setter-Schreiben
        $this->fach = $fach;
        $this->email = $mail;
        $this->note = $note;
        $this->datum = $date;
    }


    public function saveToSession()
    {
        $_SESSION['grades'][] = serialize($this);
    }


    public function saveToFile($filename)
    {
        file_put_contents($filename, serialize($this));
    }


    public static function getEntryFromFile($filename)
    {
        $gradeEntry = unserialize(file_get_contents($filename));
        return $gradeEntry;
    }


    /**
     * Getter-Methode für den Student:innen-Namen
     * @return String Der Name des Studenten als String
     */
    public function getStudiname()
    {
        return $this->studiname;
    }

    /**
     * Setzt den Studentennamen auf einen neuen Wert
     * @param String $newStudiName Der neuer Name des Studenten (wird geprüft)
     */
    public function setStudiname($newStudiName)
    {
        if (strlen($newStudiName) < 3)
        {
            throw new Exception("Name: Nicht lang genug!");
            //Alternative mit Fehlerliste als Datenfeld
            //$this->fehlerliste['studentenname'] = "Nicht lang genug!";
            //return $this->fehlerliste;
        }
        else{
            $this->studiname = $newStudiName;
            //unset($this->fehlerliste['studentenname']);
        }
    }


	/**
	 * @return string
	 */
	public function getFach(): string {
		return $this->fach;
	}
	
	/**
     * Setter-Methode...
	 * @param string $fach 
	 * @return self
	 */
	public function setFach(string $fach): self {
		$this->fach = $fach;
		return $this;
	}

}

?>