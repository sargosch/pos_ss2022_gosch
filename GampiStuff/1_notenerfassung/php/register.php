<?php
   
    session_start();
    $_SESSION = array();


    if (isset($_POST['studiname']) && isset($_POST['email']) && isset($_POST['fach']) && isset($_POST['note']) && isset($_POST['datum']))
    {
        $studiname = $_POST['studiname'];
        $note = $_POST['note'];
        $email = $_POST['email'];
        checkName($studiname);
        isValidGrade($note);
        isValidEmail($email);
       
    }
    else{
        $_SESSION['Formulardaten'] = 'Bitte alle Pflichtfelder befüllen!';
    }
   

    /**
     * Die Funktion bekommt einen zu prüfenden Wert und checkt, ob 
     * keine Ziffern vorkommen
     * @param $name String Der zu prüfende Wert
     * @param String[] Ein Array, in dem auftretende Fehler gespeichert werden.
     * @return Boolean true, wenn Prüfung erfolgreich.
     */
    function checkName($name)
    {
        //TODO: RegEx anpassen (JavaScript, SQL, etc.)
        $regex = '/\d/m';
       
        if (empty($name) || preg_match($regex,$name) === 1 || strlen($name) < 3)
        {
            $_SESSION['Name'] = 'Bitte korrekten Namen eingeben!';
            return false;
        }

        return true;
    }


    /**
     * Prüft, ob der übergebene Wert zwische 1 und 5 liegt
     * @param int $note Der zu prüfende Notengrad
     * @return Boolean true, wenn Prüfung erfolgreich
     */
    function isValidGrade($note)
    {
        $regex  = '/^[1-5]$/im';
        if (!empty($note) && preg_match($regex,$note) === 1)
        {
            return true;
        }
        $_SESSION['Note'] = 'Bitte korrekte Note eingeben!';
        return false;
    }




    function isValidEmail($email)
    {
        $regex  = '/(^[a-zA-Z0-9_.]+[@]{1}[a-z0-9]+[\.][a-z]+$)/m';
        if (!empty($email) && preg_match($regex,$email) === 1)
        {
            return true;
        }
        $_SESSION['Email'] = 'Bitte korrekte Email eingeben!';
        return false;
    }

    function isValidSubject($fach)
    {
        $regex  = '/^(POS)$|^(DBI)$|^(NVS)$|^(FSE)$/im';
        if (!empty($fach) && preg_match($regex,$fach) === 1)
        {
            return true;
        }
        $_SESSION['Fach'] = 'Bitte gültiges Fach auswählen';
        return false;
    }


    if (!empty($_SESSION))
    {

        var_dump($_SESSION);
        header('Location: ../index.php');
    }
    else
    {
            header('Location: ./success.php');
    }

?>