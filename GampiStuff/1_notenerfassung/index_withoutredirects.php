<?php
    if (isset($_POST['studiname']) && isset($_POST['email']) && isset($_POST['fach']) && isset($_POST['note']) && isset($_POST['datum']))
    {
        $fehler = array();
        $studiname = $_POST['studiname'];
        $note = $_POST['note'];
        $email = $_POST['email'];
        checkName($studiname, $fehler);
        isValidGrade($note, $fehler);
        isValidEmail($email, $fehler);
    }
    else{
        $fehler['Formulardaten'] = 'Bitte alle Pflichtfelder befüllen!';
    }
   
    /**
     * Die Funktion bekommt einen zu prüfenden Wert und checkt, ob 
     * keine Ziffern vorkommen
     * @param $name String Der zu prüfende Wert
     * @param String[] Ein Array, in dem auftretende Fehler gespeichert werden.
     * @return Boolean true, wenn Prüfung erfolgreich.
     */
    function checkName($name,&$fehler)
    {
        //TODO: RegEx anpassen (JavaScript, SQL, etc.)
        $regex = '/\d/m';
       
        if (empty($name) || preg_match($regex,$name) === 1 || strlen($name) < 3)
        {
            $fehler['Name'] = 'Bitte korrekten Namen eingeben!';
            return false;
        }

        return true;
    }


    /**
     * Prüft, ob der übergebene Wert zwische 1 und 5 liegt
     * @param int $note Der zu prüfende Notengrad
     * @param String[] Ein Array, in dem auftretende Fehler gespeichert werden.
     * @return Boolean true, wenn Prüfung erfolgreich
     */
    function isValidGrade($note,&$fehler)
    {
        $regex  = '/^[1-5]$/im';
        if (!empty($note) && preg_match($regex,$note) === 1)
        {
            return true;
        }
        $fehler['Note'] = 'Bitte korrekte Note eingeben!';
        return false;
    }

    $x=[5,6,7];

    function isValidEmail($email,&$fehler)
    {
        $regex  = '/(^[a-zA-Z0-9_.]+[@]{1}[a-z0-9]+[\.][a-z]+$)/m';
        if (!empty($email) && preg_match($regex,$email) === 1)
        {
            return true;
        }
        $fehler['Email'] = 'Bitte korrekte Email eingeben!';
        return false;
    }


    function isValidSubject($fach,&$fehler)
    {
        $regex  = '/^(POS)$|^(DBI)$|^(NVS)$|^(FSE)$/im';
        if (!empty($fach) && preg_match($regex,$fach) === 1)
        {
            return true;
        }
        $fehler['Fach'] = 'Bitte gültiges Fach auswählen';
        return false;
    }


    if (!empty($fehler))
    {

        //var_dump($fehler);
       
    }
    else
    {
            //umleiten, falls erfolgreich
            //header('Location: ./success.php');
    }

?>

<!DOCTYPE html>
<html>
<head>
    <title>HTML-PHP-Demo</title>
    <meta charset="utf8">
    <meta lang="de">
    <meta name="author" content="Michael Gamper">
    <meta name="description" content="HTML/PHP-Demo">
    <meta name="keywords" content="HTML, CSS, PHP">
    <!--link rel="stylesheet" href="css/meinstil.css"-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
</head>
<body>
    
    <section id="header" class="flex-row bg-primary justify-content-center p-4">
        <h1 class="text-light">Notenerfassung HTL</h1>
        <div></div>
    </section>

    <?php
    
    if (!empty($fehler))
        echo '<section id="header" class="flex-row bg-danger justify-content-center p-4">';
        foreach ($fehler as $key => $value) {
            echo '<p class="text-bg-danger">'.htmlspecialchars($value)."</p>";
        }
        echo '</section>';
    ?>
    <section id="content" class="container">

        <form action="index_withoutredirects.php" method="POST" id="form">

            <div class="input-group m-4">
                <label class="input-group-text" for="studiname">Name Studierende/r*</label>
                <input class="form-control" name="studiname" type="text" required="" placeholder="Maxine Musterfrau" />
            </div>

            <div class="input-group m-4">
                <label class="input-group-text" for="email">Email</label>
                <input class="form-control" name="email" type="email" placeholder="maxine@tsn.at" />
            </div>

            <div class="input-group m-4">
                <label class="input-group-text" for="fach">Fach*</label>
                <select class="form-control" name="fach" required="">
                    <option value="POS" selected="">POS</option>
                    <option value="DBI">DBI</option>
                    <option value="NVS">NVS</option>
                    <option value="FSE">FSE</option>
                </select>
            </div>
        
            <div class="input-group m-4">
                <label class="input-group-text" for="note">Note*</label>
                <input class="form-control" name="note" type="number" min="1" max="5" placeholder="1"/>
            </div>
        
            <div class="input-group m-4">
                <label class="input-group-text" for="datum">Prüfungsdatum*</label>
                <input class="form-control" id="datum" name="datum" type="date" required="" />
            </div>
          
            <!--TODO: Client-seitige Datenvalidierung mit JS umsetzen -->

            <input class="form-control btn btn-primary m-4" id="submitbutton" type="submit" value="Validieren"/>  
        </form>
    </section>
    <script src="js/valid.js"></script>
</body>
</html> 