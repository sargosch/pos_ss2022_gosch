<?php
    session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <title>HTML-PHP-Demo</title>
    <meta charset="utf8">
    <meta lang="de">
    <meta name="author" content="Michael Gamper">
    <meta name="description" content="HTML/PHP-Demo">
    <meta name="keywords" content="HTML, CSS, PHP">
    <!--link rel="stylesheet" href="css/meinstil.css"-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    
    <section id="header" class="flex-row bg-primary justify-content-center p-4">
        <h1 class="text-light">Notenerfassung HTL</h1>
        <div></div>
    </section>
    <?php
    
    if (!empty($_SESSION))
    {
        echo '<section id="header" class="flex-row bg-danger justify-content-center p-4">';
        foreach ($_SESSION as $key => $value) {
            echo '<p class="text-bg-danger">'.htmlspecialchars($value)."</p>";
        }
        echo '</section>';
    }
    ?>


    
    <section id="content" class="container">

        <form action="php/register.php" method="POST" id="form" class="">

            <div class="input-group m-4">
                <label class="input-group-text" for="studiname">Name Studierende/r*</label>
                <input class="form-control" name="studiname" type="text" required="" placeholder="Maxine Musterfrau" />
            </div>
            
            <div class="input-group m-4">
                <label class="input-group-text" for="email">Email</label>
                <input class="form-control" name="email" type="email" placeholder="maxine@tsn.at" />
            </div>
            
            <div class="input-group m-4">
                <label class="input-group-text" for="fach">Fach*</label>
                <select class="form-control" name="fach" required="">
                    <option value="POS" selected="">POS</option>
                    <option value="DBI">DBI</option>
                    <option value="NVS">NVS</option>
                    <option value="FSE">FSE</option>
                </select>
            </div>

            <div class="input-group m-4">
                <label class="input-group-text" for="note">Note*</label>
                <input class="form-control" name="note" type="number" min="1" max="5" placeholder="1"/>
            </div>

            <div class="input-group m-4">
                <label class="input-group-text" for="datum">Prüfungsdatum*</label>
                <input id="datum" class="form-control" name="datum" type="date" required="" />
            </div>
            <!--TODO: Client-seitige Datenvalidierung mit JS umsetzen -->

            <input id="submitbutton" class="form-control btn btn-primary m-4" type="submit" value="Validieren"/>
            
        </form>
    </section>
<script src="js/validate.js"></script>
</body>
</html> 