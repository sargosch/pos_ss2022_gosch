let datum = document.getElementById('datum');
datum.onchange = function()
{
    validateExamDate(this);
}

let submitButton = document.getElementById('submitbutton');
let form = document.getElementById('form');
console.log(form);
form.onsubmit = function(e)
{
    e.preventDefault();
    if (validateExamDate(datum))
    {
        this.submit();
    }
}

function validateExamDate(elem)
{
    let aktuellesDatum = new Date();
    //console.log(aktuellesDatum);

    let examDate = new Date(elem.value);

    if (examDate <= aktuellesDatum)
    {
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
        return true;
    }
    else{
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
        return false;
    }
}