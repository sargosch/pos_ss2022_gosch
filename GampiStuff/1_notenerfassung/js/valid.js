let datumsEl = document.getElementById("datum");
//console.log(datumsEl);

let formEl = document.getElementById('form');
console.log(formEl);

datumsEl.onchange = function()
{
    checkDatum(datumsEl);
}


formEl.onsubmit = function()
{
    formEl.preventDefault();
    if (checkDatum(datumsEl))
    {
        formEl.submit();
    }
    else{
        //Formular nicht abschicken
    }
}


function checkDatum(elem)
{
    let aktuellesDatum = new Date();
    //console.log(aktuellesDatum);

    let formDatum = new Date(elem.value);
    if (formDatum <= aktuellesDatum)
    {
        console.log("Super");
        elem.style="1px solid green";
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
        return true;
    }
    else{
        console.log("Nicht super!");
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
        return false;
    }


}

