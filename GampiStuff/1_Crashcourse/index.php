<?php
    //Starte eine neue oder hole aktuelle Session
    session_start();
?>

<!DOCTYPE html>
<html lang="de"></html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrierung</title>
</head>
<div id="fehler">
    <?php
        //Wenn in der Session ein Fehler-Eintrag existiert, gib alle Fehler des Arrays aus
        if(isset($_SESSION['fehler']))
        {
            foreach ($_SESSION['fehler'] as $key => $value) {
                echo "<p>$key: $value</p>";
            }

        }
    ?>
</div>
<body>
    <form method="post" action="php/register.php">
        <input type="text" name="vorname" required="" placeholder="Max" value="<?php if(isset($_SESSION['fehler'])) {echo htmlspecialchars($_SESSION['vorname']);} ?>"/>
        <input type="text" name="nachname" required="" placeholder="Mustermann" value="<?php if(isset($_SESSION['fehler'])) {echo htmlspecialchars($_SESSION['nachname']);} ?>"/>
        <input type="email" name="email" required="" placeholder="max@test.de"  value="<?php if(isset($_SESSION['fehler'])) {echo htmlspecialchars($_SESSION['email']);} ?>"/>
        <input type="password" name="passwort1" required=""/>
        <input type="password" name="passwort2" required=""/>
        <input type="date" name="geburtsdatum" required=""  value="<?php if(isset($_SESSION['fehler'])) {echo htmlspecialchars($_SESSION['geburtsdatum']);} ?>"/>
        <input type="submit" value="Registrieren" required=""/>
    </form>
</body>
</html>
