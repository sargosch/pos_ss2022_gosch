<?php


/**
 * Prüft, ob der Parameterwert keine Ziffern enthält und mindestens drei Zeichen lang ist.
 * @param string $possibleName Der zu prüfende Wert
 * @return boolean false, wenn Validierung fehlschlägt.
 */
function validatePersonName($possibleName)
{
    $pattern = '/\d/';
    //Suche nach Ziffern oder sehr kurzen Eingaben
    if (preg_match($pattern, $possibleName) === 1 || strlen($possibleName)<3)
    {
        return false;
    }
    return true;
}


/**
 * TODO: Funktionen kommentieren
 */
function validateBirthdate($birthdate)
{
    if ( (strtotime($birthdate) === false) || strtotime($birthdate) > time())
    {
        return false;
    }
    return true;
}


function validatePWStrength($passwortkandidat)
{
    $pattern = '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!?-_.:,;+*~#´`\\={}()\/&%$§"])[A-Za-z0-9!?-_.:,;+*~#´`\\={}()\/&%$§"]{8,32}$/';
    if (preg_match($pattern, $passwortkandidat) === 1)
    {
        return true;
    }
    return false;
}


function checkIfPasswordMatch($passwort1, $passwort2)
{
    if ($passwort1 == $passwort2)
    {
        return true;
    }
    return false;
}

?>
