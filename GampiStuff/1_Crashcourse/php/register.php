<?php
    session_start();

    require_once('functions.inc.php');

    //In diese Variable werden auftretende Validierungsfehler geschrieben
    $fehler = array();


    //Todo: Prüfen, ob alle Formularfelder übermittelt wurden
    if(isset($_POST['vorname']) && isset($_POST['nachname']) && isset($_POST['email']) && isset($_POST['passwort1']) && isset($_POST['passwort2']) && isset($_POST['geburtsdatum']))
    {
      
        //Hole die Werte aus dem Formular (ungeprüft)
        $vorname = $_POST['vorname'];
        $nachname = $_POST['nachname'];
        $email = $_POST['email'];
        $passwort1 = $_POST['passwort1'];
        $passwort2 = $_POST['passwort2'];
        $geburtsdatum = $_POST['geburtsdatum'];

        //Validiere alle Werte

        //TODO: Validierung der Vor-/Nachnamen


        if (validatePersonName($vorname) === false)
        {
            $fehler["Vorname"] = "Bitte einen gültigen Vornamen eingeben.";
        }

        if (validatePersonName($nachname) === false)
        {
            $fehler["Nachname"] = "Bitte einen gültigen Nachnamen eingeben.";
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
        {
            //echo "Email-Validierung fehlgeschlagen";
            $fehler["email"] = "Email-Validierung fehlgeschlagen";
        }
        
        if (validatePWStrength($passwort1) === false)
        {
            $fehler["Passwort-Staerke"] = "Passwort nicht stark genug!";
        }

        if (checkIfPasswordMatch($passwort1, $passwort2) === false)
        {
            $fehler["Passwort-Uebereinstimmung"] = "Passworte stimmen nicht überein!";
        }

        
        if (validateBirthdate($geburtsdatum) === false)
        {
            $fehler["Geburtsdatum"] = "Bitte gültiges Geburtsdatum eingeben.";
        } 

    }//Ende IF isset...
    else
    {
        $fehler["Formular"] = "Bitte alle Felder ausfüllen!";
    }
    
    
    if(sizeof($fehler) > 0)
    {
        //var_dump($fehler);
        //Fehler an index.php übergeben.
        $_SESSION = array();
        $_SESSION['fehler'] = $fehler;
        $_SESSION['vorname'] = $vorname;
        $_SESSION['nachname'] = $nachname;
        $_SESSION['email'] = $email;
        $_SESSION['geburtsdatum'] = $geburtsdatum;
        header('Location: ../index.php');
    }
    else{
        $_SESSION = array();
        session_destroy();
        //Alles fertig.
        header('Location: success.php');
    }
    
    
   



?>