<?php
    $benutzer = null;
    if (isset($_POST['email']) && isset($_POST['passwort']))
    {
        echo "Loginversuch";

        $email = $_POST['email'];
        $pw = $_POST['passwort'];
        //$benutzer = Benutzer::get($email, $pw);
        
        if ($benutzer === null)
        {
            //Benutzername oder PW falsch
        }
        else{
            //Mit Benutzer weiterarbeiten
            $benutzer->login();
        }

    }

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
    <title>Wochenkarte</title>
</head>
<body>

    <nav class="navbar navbar-expand-lg bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Mensa HTL Imst</a>
                <form class="d-flex" role="login" method="POST">
                    <input class="form-control me-2 m-2" type="email" placeholder="Email" aria-label="Email" name="email">
                    <input class="form-control me-2 m-2" type="password" placeholder="***" aria-label="Password" name="passwort">
                    <button class="btn btn-primary m-2" type="submit">Login</button>
                </form>
        </div>
    </nav>

  
    <div class="container text-center">

    <?php
        if ($benutzer!== null && $benutzer->isLoggedIn())
        {
            // echo HTML für Wochenkarte
        }
        else
        {
            // echo HTML für Bitte einloggen-Meldung
        }

    ?>
        <h1>Wochenkarte</h1>
        <div class="row">
            <div class="col-sm-4">
                <h2>Montag</h2>
                <img src="img/montag.png" class="img-thumbnail" alt="Montag">
            </div>  
            <div class="col-sm-4">
            <h2>Dienstag</h2>
                <img src="img/dienstag.png" class="img-thumbnail" alt="Dienstag">
            </div>
            <div class="col-sm-4">
                <h2>Mittwoch</h2>
                <img src="img/mittwoch.png" class="img-thumbnail" alt="Mittwoch">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h2>Donnerstag</h2>
                <img src="img/donnerstag.png" class="img-thumbnail" alt="Donnerstag">
            </div>
            <div class="col-sm-4">
                <h2>Freitag</h2>
                <img src="img/montag.png" class="img-thumbnail" alt="Freitag">
            </div>
        </div>
    </div>
   

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Datenschutz ist uns wichtig!</h1>
            </div>
            <div class="modal-body">
                Diese Seite verwendet Cookies, aus Gründen!
                <p>Stimmen Sie der Verwendung zu?</p>
                <p>Wenn nein, können Sie unsere Dienste leider nicht nutzen.</p>
            </div>
            <div class="modal-footer">
                <form method="POST">
                    <button type="submit" name="cookies" value="yesplease" class="btn btn-secondary" data-bs-dismiss="modal">Ja</button>
                </form>
            </div>
            </div>
        </div>
    </div>


    <script>
        const myModal = new bootstrap.Modal("#staticBackdrop", {keyboard: false});
        myModal.show();
    </script>
       

</body>
</html>