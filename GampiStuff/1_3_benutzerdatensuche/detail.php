<?php

require_once('php/functions.inc.php');

?>

<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Userdetails</title>
</head>
<h1>Userdetails:</h1>
<body class="container">
    <a href="index.php">Zurück</a>
    <?php

        if (isset($_GET['id']) && is_numeric($_GET['id']))
        {
            echoUserDetail(getDataPerId($_GET['id']));
        }

    ?>  

</body>
</html>