<?php

require_once('php/functions.inc.php');

?>

<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Benutzerdaten anzeigen</title>
</head>
<body class="container">
    <h1>Benutzerdaten anzeigen</h1>
    <form action="index.php" method="GET">
        <div class="input-group mb-3">
            
            <label for="suche" class="input-group-text">Suche: </label>
            <input type="text" class="form-control" name="suche" placeholder="Suchwort"  minlength="1" maxlength="15" value="<?php echo $wert = isset($_GET['suche']) ? htmlspecialchars(filter_input(INPUT_GET, 'suche', FILTER_DEFAULT)) : "" ?>"/>
            <input type="submit" class="btn btn-outline-primary" value="Suchen" />
            <a class="btn btn-outline-secondary" href="index.php">Leeren</a>
        </div>
        
    </form>

    <?php

        //Reagieren auf GET-Requests

        if (isset($_GET['suche']))
        {
            $suchstring = filter_input(INPUT_GET, 'suche', FILTER_DEFAULT);
            echoUserTable(getFilteredData($suchstring));
        }
        else{
            echoUserTable(getAllData());
        }

    ?>

</body>
</html>