<?php


/**
 * Liefert alle User als geschachteltes Array zurück
 * @return array Ein indiziertes Array aller Benutzer
 */
function getAllData()
{
    require('daten/userdata.php');
    return $data;
}


/**
 * Liefert einen Benutzer als Array zurück
 * @param $id int Die ID des gesuchten Benutzers
 * @return array|false Ein Array, das alle Benutzerinfos enthält. Oder false, wenn nichts gefunden wurde.
 */
function getDataPerId($id)
{
    require('daten/userdata.php');

    foreach($data as $user)
    {
        if ($user['id'] == $id)
        {
            return $user;
        }
    }
}


/**
 * Liefert alle Benutzer zurück, die dem Suchstring entsprechen (Vorname, Nachname und Email werden durchsucht)
 * @param string Der String, nach dem gesucht wird
 * @return array Ein Array, der die Suchergebnisse beinhaltet
 */
function getFilteredData($suchstring)
{
    require('daten/userdata.php');

    $filteredData = array();
    $suchstring = strtolower($suchstring);
    foreach($data as $user)
    {
        if ( str_contains(strtolower($user['email']), $suchstring) || str_contains(strtolower($user['firstname']), $suchstring) ||  str_contains(strtolower($user['lastname']), $suchstring))
        {
            array_push($filteredData, $user);
        }
    }
    return $filteredData;

}


/**
 * Gibt die User-Tabelle als HTML-Fragment aus
 * @param array $userdata Ein array mit den auszugebenden Daten
 * @todo: Kopplung verringern
 */
function echoUserTable($userdata)
{
    $tableHTML = '';
    //Wenn im Array Daten stehen...
    if(is_array($userdata) && sizeof($userdata)>0)
    {
        $tableHTML .= '<table class="table table-striped">';
        $tableHTML .= '<tr>';
 
        foreach($userdata[0] as $key => $value)
        {
            $tableHTML .= "<th>$key</th>";
        }
        $tableHTML .= '</tr>';
        foreach($userdata as $key => $user)
        {
            $tableHTML .= '<tr>';
            $tableHTML .= '<td><a href="detail.php?id='.$user['id'].'">'.$user['id'].'</a></td>';
            $tableHTML .= '<td>'.$user['firstname'].'</td>';
            $tableHTML .= '<td>'.$user['lastname'].'</td>';
            $tableHTML .= '<td>'.$user['email'].'</td>';
            $tableHTML .= '<td>'.$user['phone'].'</td>';
            $tableHTML .= '<td>'.$user['birthdate'].'</td>';
            $tableHTML .= '<td>'.$user['street'].'</td>';
            $tableHTML .= '</tr>';
        }
        $tableHTML .= '</table>';
       
    } 
    else //Im Fehlerfall...
    {
        $tableHTML= '<div class="alert alert-danger"><h2>Leider keine Daten zum Suchbegriff gefunden.</h2></div>';
    }

    echo ($tableHTML);

}


/**
 * Gibt alle Details zum User als HTML-Tabelle aus
 * @param array $userdata Die Benutzerdaten
 * @todo Kopplung verringern
 */
function echoUserDetail($userdata)
{
    $tableHTML = '';
    //Wenn im Array Daten stehen...
    if(is_array($userdata) && sizeof($userdata)>0)
    {
        $tableHTML .= '<table class="table table-hover">';
        foreach($userdata as $key => $user)
        {
            $tableHTML .= '<tr>';
            $tableHTML .= '<th>'.strtoupper($key).'</th>';
            $tableHTML .= '<td>'.$user.'</td>';
            $tableHTML .= '</tr>';
        }
        $tableHTML .= '</table>';
       
    } 
    else //Im Fehlerfall...
    {
        $tableHTML= '<h2>Leider keine Daten zum Suchbegriff gefunden.</h2>';
    }

    echo ($tableHTML);

}


?>