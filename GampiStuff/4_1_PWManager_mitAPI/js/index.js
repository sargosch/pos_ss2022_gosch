//document.onload = function(){};
document.addEventListener('DOMContentLoaded', function(e)
{
    let btnSearch = document.getElementById('btnSearch');
    let btnClear = document.getElementById('btnClear');
    let inputFilter = document.getElementById('filter');

    //Falls auf den Button geklickt wird...
    btnSearch.onclick = function()
    {
        let inhalt = inputFilter.value;
        loadFilteredCredentials();
    }

    btnClear.addEventListener('click', function(){
        inputFilter.value = "";
        loadAllCredentials();
    })

  

})



/**
 * Baue aus JSON-Parameter eine den HTML-Code für eine Tabelle
 * @param {} data Credentials als JSON-Standardobjekte
 * @returns string Die HTML-Tabellenzeilen als String
 */
function parseCredentialsTable(data){

    let htmlTabelleninhalt = '';


    data.forEach(element => {
        htmlTabelleninhalt += "<tr>";
        htmlTabelleninhalt += "<td>";
        htmlTabelleninhalt += element.name;
        htmlTabelleninhalt += "</td>";
        htmlTabelleninhalt += "<td>";
        htmlTabelleninhalt += element.domain;
        htmlTabelleninhalt += "</td>";
        htmlTabelleninhalt += "<td>";
        htmlTabelleninhalt += element.cms_username;
        htmlTabelleninhalt += "</td>";
        htmlTabelleninhalt += "<td>";
        htmlTabelleninhalt += element.cms_password;
        htmlTabelleninhalt += "</td>";
        htmlTabelleninhalt += "</tr>";
    });
    return htmlTabelleninhalt;
};


function loadAllCredentials(){
    //AJAX-Request auf fertiges Webservice/API
    //jQuery, oder
    //VanillaJS, oder
    //fetch api

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'api/credentials', true);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.responseType = 'json';
    xhr.onreadystatechange= function(){
        
        if (xhr.status == 200 && xhr.readyState == 4)
        {
            //console.log(xhr.response);
            //JSON.parse(xhr.response)
            let tbodyElemet = document.getElementById('credentials');
            //console.log(xhr.response);
            tbodyElemet.innerHTML = parseCredentialsTable(xhr.response);
        }
    };
    xhr.send();

};


/**
 * 
 */
function loadFilteredCredentials(){
    let inputFilter = document.getElementById('filter');
    let inhalt = inputFilter.value;
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'api/credentials/suche/' + inhalt, true);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.responseType = 'json';
    xhr.onreadystatechange= function(){
        //Wenn Ergebnisse vorliegen...
        if (xhr.status == 200 && xhr.readyState == 4)
        {
            console.log(xhr.response);
            //Oder: JSON.parse(xhr.response)

            //Ändere den Inhalt der Webseite (die Tabelle)
            let tbodyElemet = document.getElementById('credentials');
            //console.log(xhr.response);
            tbodyElemet.innerHTML = parseCredentialsTable(xhr.response);
        }
    };
    xhr.send();



}