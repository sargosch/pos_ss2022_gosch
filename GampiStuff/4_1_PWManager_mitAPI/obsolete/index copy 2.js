//Falls das HTML komplett geladen wurde...
document.addEventListener('DOMContentLoaded', function(e)
{

    //loadAllCredentials();
    //loadFilteredCredentials();

    //Hole Suchbutton
    let suchButton = document.getElementById("btnSearch");

    //Registriere Event-Handler für den Linksklick auf den Suchbutton
    suchButton.onclick = function()
    {
        loadFilteredCredentials();
    }

    //Registriere Event-Handler für den Linksklick
    let resetButton = document.getElementById("btnClear");
    resetButton.onclick = function()
    {
        //Hole das Input-Element und leere den Inhalt
        let filterInput = document.getElementById("filter");
        filterInput.value = "";
        loadAllCredentials();
    }

    //Hole das Input-Element und registriere ein KeyUp-Event (sobald jemand eintippt)
    let filterInput = document.getElementById("filter");
    filterInput.onkeyup = function()
    {
        loadFilteredCredentials();
    }

    //Wenn seite fertig geladen wurde...
    loadAllCredentials();

})


/**
 * Parse die kompletten JSON-Daten und baue daraus HTML
 * @param {*} data Ein JSON-Objekt mit mehreren Credentials
 * @returns ein String, der HTML beinhaltet
 */
function parseCredentialsTable(data)
{
    let zeilenHTML = '';
    //Für jedes Element im JSON-Array...
    //Alternative: Klassische for-Schleife
    data.forEach(element => {
        zeilenHTML += "<tr>";
        zeilenHTML += "<td>";
        zeilenHTML += element.name;
        zeilenHTML += "</td>";
        zeilenHTML += "<td>";
        zeilenHTML += element.domain;
        zeilenHTML += "</td>";
        zeilenHTML += "<td>";
        zeilenHTML += element.cms_name;
        zeilenHTML += "</td>";
        zeilenHTML += "<td>";
        zeilenHTML += element.cms_password;
        zeilenHTML += "</td>";
        zeilenHTML += "<tr>";
    });

    return zeilenHTML;
}

/**
 * Lade über einen XHR asynchron Credentials von der API
 */
function loadAllCredentials()
{
   let xhr = new XMLHttpRequest();
   //Definiere den Endpoint und die Methode
   xhr.open('GET', 'api/credentials', true);
   //Zur Interpretation des Ergebnisses als JSON-Objekt
   xhr.responseType = 'json';
   //Sende der API, dass der Client JSON erwartet
   xhr.setRequestHeader('Accept', 'application/json');
   //Registriere einen Event-Handler. Wird aufgerufen, sobald eine Antwort vom Server kommt (also asynchroner Code, so wie jeder Event-Handler/jede Callback-Function)
   xhr.onload = function()
   {
            //Wenn der HTTP-Status passt und json zurückgekommen ist
            if (xhr.status == 200 && xhr.responseType == 'json'){
                console.log(xhr.response);
                let data = xhr.response;
                //Hole das Table-DOM-Element und überschreibe dessen Inhalt
                let divTablebody = document.getElementById('credentials');
                divTablebody.innerHTML = parseCredentialsTable(data);
                
            }
   }
   //Sende den Request an die API
   xhr.send();

}


/**
 * Lade über einen XHR asynchron Credentials von der API, die einem Suchstring entsprechen
 */
function loadFilteredCredentials()
{
    //Hole den Inhalt des Filter-Inputs
    let filterInput = document.getElementById("filter");
    let filter = filterInput.value;
    //filter = "TSN";
    if (filter == "")
    {
        loadAllCredentials();
    }
    else{
        //siehe loadAllCredentials, nur dass jetzt ein anderer Endpoint aufgerufen wird
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'api/credentials/suche/'+filter, true);
        xhr.responseType = 'json';
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.onload = function()
        {
                 if (xhr.status == 200 && xhr.responseType == 'json'){
                     console.log("Filterddata da:");
                     console.log(xhr.response);
                     let data = xhr.response;
                     //Hole das Table-DOM-Element und überschreibe dessen Inhalt
                     let divTablebody = document.getElementById('credentials');
                     divTablebody.innerHTML = parseCredentialsTable(data);
                 }
        }
        xhr.send();
    }
}


