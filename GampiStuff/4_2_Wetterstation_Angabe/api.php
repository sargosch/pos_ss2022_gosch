<?php
require_once('models/Measurement.php');
require_once('models/Station.php');
require_once('controllers/RESTController.php');

/*
 * Dieses Skript erledigt das Routing. Abhängig von der URL wird der richtige Teil der Anwendung geladen
 */
//Falls der GET-Parameter r existiert, soll geroutet werden
if (isset($_GET['r']))
{
    $route = explode('/',$_GET['r']);
    //var_dump($route);
}
else{
    //Die Standardroute, falls r nicht gesetzt ist.
    $route = ['stations'];
}

//Hole den Namen des Controllers aus der Route
if (sizeof($route) > 0)
{
    $controller = $route[0];
}

//Wenn Credentials gefragt sind...

if ($controller == 'stations')
{
    echo "StationsController aufrufen!";
}
elseif($controller == 'measurements')
{
    echo "Measurementscontroller aufrufen!";
}
else{
    RESTController::responseHelper('REST-Controller '. $controller. ' not found', 404);
}