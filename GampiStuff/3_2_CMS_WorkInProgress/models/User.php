<?php
require_once('DatabaseObject.php');
require_once('Database.php');
class User implements DatabaseObject
{
    private $uid;
    private $uname;
    private $upwhash;

    public function create()
    {

    }

    public function update()
    {

    }

    public static function getAll()
    {

        try{
            $db = Database::connect();
            //$db = new PDO('','','');
            $query = 'SELECT * FROM t_users';
            $stmt = $db->query($query);
            var_dump($stmt->fetchAll());
        }
        catch(PDOException $ex)
        {
            echo $ex->getMessage();
        }

        
    }


    public static function get($id)
    {

    }


    public static function delete($id)
    {

    }

    /**
     * Get the value of uid
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set the value of uid
     */
    public function setUid($uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get the value of uname
     */
    public function getUname()
    {
        return $this->uname;
    }

    /**
     * Set the value of uname
     */
    public function setUname($uname): self
    {
        $this->uname = $uname;

        return $this;
    }

    /**
     * Get the value of upwhash
     */
    public function getUpwhash()
    {
        return $this->upwhash;
    }

    /**
     * Set the value of upwhash
     */
    public function setUpwhash($upwhash): self
    {
        $this->upwhash = $upwhash;

        return $this;
    }
}


?>
