<?php
require_once('Database.php');
require_once('DatabaseObject.php');
class Article  
{
    private $aid;
    private $atitle;
    private $atext;
    private $acreationdate;
    private $uid;
    private $apublicationdate;


    /**
     * Funktion zum Speichern von neuen Beiträgen in der Persistenzschicht (DB)
     * @throws Ausnahme, falls das Speichern in die DB nicht funktioniert.
     * @todo Testing und alle Eventualitäten bei DB-Zugriff abfangen
     */
    public function create()
    {
        try{
            $dbconn = Database::connect();
            //$dbconn = new PDO('','');
            //Artikel existiert noch nicht...
            //Todo: User-Id aus Session holen und in INSERT einbauen
            $query = 'INSERT INTO t_articles VALUES(null,:title,:text,DEFAULT,:uid,:pdate)';

            //Bereite Query vor...
            $ps = $dbconn->prepare($query);

            //Binde Platzhalte an Werte
            $ps->bindValue('title', $this->atitle,PDO::PARAM_STR);
            $ps->bindValue('text', $this->atext, PDO::PARAM_STR);
            $ps->bindValue('pdate', $this->apublicationdate, PDO::PARAM_STR);
            $ps->bindValue('uid', $this->uid, PDO::PARAM_INT);

            //Führe Statement aus...
            $ps->execute();

            //Hole von DB vergebene ID und setze ID des Objekts
            $id = $dbconn->lastInsertId();
            $this->setAid($id);

            Database::disconnect();

            //Debugausgabe
            //var_dump($this);

        }
        catch(Exception $e)
        {
            //Fehler an aufrufer weiterwerfen
            throw $e;
        }
    
    }



    public function update()
    {

    }


    public static function getAll()
    {
        try{
            $dbconn = Database::connect();
            //$dbconn = new PDO('','');
           
            //Alle Artikel auslesen
            $query = 'SELECT * FROM t_articles';

            //Bereite Query vor...
            $ps = $dbconn->query($query);
            
            $alleArtikel = $ps->fetchAll(PDO::FETCH_CLASS, 'Article');
            return $alleArtikel;
            
        }
        catch (Exception $e){
            throw $e;
        }

    }


    public static function get($id)
    {
        try{
            $dbconn = Database::connect();
            //$dbconn = new PDO('','');
           
            //Alle Artikel auslesen
            $query = 'SELECT * FROM t_articles WHERE aid = ?';

            //Bereite Query vor...
            $ps = $dbconn->prepare($query);
            //Binde Werte und Typen an Platzhalter...
            $ps->bindValue(1, $id, PDO::PARAM_INT);
            $ps->execute();
            //Hole Zeile als Objekt und mappe zu Article-Objekt...
            $article = $ps->fetchObject('Article');
            return $article;
            
        }
        catch (Exception $e){
            throw $e;
        }
        
    }


    public static function delete($id)
    {

    }



    /**
     * Get the value of aid
     */
    public function getAid()
    {
        return $this->aid;
    }

    /**
     * Set the value of aid
     */
    public function setAid($aid): self
    {
        $this->aid = $aid;

        return $this;
    }

    /**
     * Get the value of atitle
     */
    public function getAtitle()
    {
        return $this->atitle;
    }

    /**
     * Set the value of atitle
     */
    public function setAtitle($atitle): self
    {
        $this->atitle = $atitle;

        return $this;
    }

    /**
     * Get the value of atext
     */
    public function getAtext()
    {
        return $this->atext;
    }

    /**
     * Set the value of atext
     */
    public function setAtext($atext): self
    {
        $this->atext = $atext;

        return $this;
    }

    /**
     * Get the value of acreationdate
     */
    public function getAcreationdate()
    {
        return $this->acreationdate;
    }

    /**
     * Set the value of acreationdate
     */
    public function setAcreationdate($acreationdate): self
    {
        $this->acreationdate = $acreationdate;

        return $this;
    }

    /**
     * Get the value of uid
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set the value of uid
     */
    public function setUid($uid): self
    {
        $this->uid = $uid;

        return $this;
    }

   

    /**
     * Get the value of apublicationdate
     */
    public function getApublicationdate()
    {
        return $this->apublicationdate;
    }

    /**
     * Set the value of apublicationdate
     */
    public function setApublicationdate($apublicationdate): self
    {
        $this->apublicationdate = $apublicationdate;

        return $this;
    }

  

}