<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <h2>Beitrag anzeigen</h2>

    <p>
        <a class="btn btn-primary" href="update.php?id=29">Aktualisieren</a>
        <a class="btn btn-danger" href="delete.php?id=29">Löschen</a>
        <a class="btn btn-default" href="index.php">Zurück</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Titel</th>
            <td>Titel 1</td>
        </tr>
        <tr>
            <th>Freigabedatum</th>
            <td>2017-02-05</td>
        </tr>
        <tr>
            <th>Besitzer</th>
            <td>User 1</td>
        </tr>
        <tr>
            <th>Inhalt</th>
            <td>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
</body>
</html>