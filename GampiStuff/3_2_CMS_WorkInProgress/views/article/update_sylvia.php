<!DOCTYPE html>

<html lang="de">
<?php

include "../helper/head.php";
require_once('../../models/Article.php');
require_once('../../models/User.php');


if (isset($_GET['id']))

{

    $article = Article::get($_GET['id']);

    $user = User::get($article->getUid());
    
    $id = $article->getAid();

}

 

 
echo "HALLOO";
if(isset($_POST['aid']))

{

  

    // $article = Article::get($_POST['aid']);

    // $article->setAtitle(isset($_POST['title'])? $_POST['title'] : $article->getAtitle());

    // $article->setAcreationdate(isset($_POST['releasedate']) ? $_POST['releasedate'] : $article->getAcreationdate());

    // $article->setUid(isset($_POST['owner']) ? $_POST['owner'] : $article->getUid());

    // $article->setAtext(isset($_POST['content']) ? $_POST['content'] : $article->getAtext());

 

    // $article->update();

    //header('Location: index.php');

    exit();

}

else{

    echo "FAIL";

}

 

 

?>

 

<body>

 

    <?php

    include "../helper/navbar.php";

    ?>

 

    <div class="container">

        <div class="row">

            <h2>Beitrag bearbeiten</h2>

        </div>

 

        <form class="form-horizontal" action="update_sylvia.php" method="post">

 

            <div class="row">

                <div class="col-md-5">

                    <div class="form-group required">

                        <label class="control-label">Titel *</label>

                        <input type="text" class="form-control" name="title" maxlength="45"

                            value="<?= htmlspecialchars($article->getAtitle()) ?>">

                    </div>

                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">

                    <div class="form-group required">

                        <label class="control-label">Freigabedatum *</label>

                        <input type="date" class="form-control" name="releasedate"

                            value="<?= htmlspecialchars(date('Y-m-d', strtotime($article->getAcreationdate()))) ?>">

                    </div>

                </div>

                <div class="col-md-1"></div>

                <div class="col-md-3">

                    <div class="form-group required">

                        <label class="control-label">Besitzer *</label>

                        <select class="form-control" name="owner">

                            <option value="<?= htmlspecialchars($article->getUId()) ?>" selceted> </option>

                            <?php

                            $users = User::getAll();

                            foreach ($users as $user) {

                                $uid = $user->getUid();

                                echo "<option value='$uid'>" . $user->getUname() . "</option>";

                            }

                            ?>

                        </select>

                    </div>

                </div>

            </div>

 

            <div class="row">

                <div class="col-md-12">

                    <div class="form-group required">

                        <label class="control-label">Inhalt *</label>

                        <textarea class="form-control" name="content"

                            rows="10" > <?= htmlspecialchars($article->getAtext()) ?></textarea>

                    </div>

                </div>

            </div>

            <div class="form-group">

                <input type="hidden" id="aid" value="<?=$article->getAid()?>">

                <button type="submit" value="submit" class="btn btn-primary">Aktualisieren</button>

                <a class="btn btn-default" href="index.php">Abbruch</a>

            </div>

        </form>

 

    </div> <!-- /container -->

</body>

 

</html>

 