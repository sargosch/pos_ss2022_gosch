<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
require_once('../../models/Article.php');
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <div class="row">
        <h2>Beiträge</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Titel</th>
                <th>Inhalt</th>
                <th>Besitzer</th>
                <th>Freigabedatum</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php
                    //var_dump(Article::getAll());
                    $artikelliste = Article::getAll();
                    
                    foreach ($artikelliste as $artikel) {
                        echo '<tr>';
                        echo '<td>'.$artikel->getaTitle().'</td>';
                        echo '<td>'.$artikel->getaText().'</td>';
                        echo '<td>'.$artikel->getUId().'</td>';
                        echo '<td>'.$artikel->getacreationdate().'</td>';
                        echo '<td><a class="btn btn-info" href="view.php?id='.$artikel->getaid().'"><span class="glyphicon glyphicon-eye-open"></span></a><a
                                class="btn btn-primary" href="update_sylvia.php?id='.$artikel->getaid().'"><span
                                class="glyphicon glyphicon-pencil"></span></a><a
                                class="btn btn-danger" href="delete.php?id='.$artikel->getaid().'"><span
                                class="glyphicon glyphicon-remove"></span></a>
                            </td>'; 
                    }
                   
                ?>
            <!-- <tr>
                <td>Beitrag 4</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisici elit...</td>
                <td>User 1</td>
                <td>2017-02-05</td>
                <td><a class="btn btn-info" href="view.php?id=29"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                            class="btn btn-primary" href="update.php?id=29"><span
                                class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                            class="btn btn-danger" href="delete.php?id=29"><span
                                class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr> -->
            </tbody>
        </table>
    </div>
</div> <!-- /container -->
</body>
</html>