<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
require_once('../../models/Article.php');
?>

<body>

<?php
include "../helper/navbar.php";

//Testen der Klasse Article (noch ohne Formulardaten)

// $artikel1->setAtitle('Breaking news: Geld verdienen leicht gemacht!');
// $artikel1->setAtext('Wir als high perfomer company bla bla...');
// $artikel1->setaPublicationdate('2023-03-15');
// $artikel1->setUid(1);
// $artikel1->create();

    //Hole Formulardaten...
    if (isset($_POST['releasedate']) && isset($_POST['title']) && isset($_POST['content']))
    {

        try{
            $artikel1= new Article();
            $artikel1->setAtitle($_POST['title']);
            $artikel1->setAtext($_POST['content']);
            //Todo: User-Id aus Session oder sonstwoher holen. Derweil Standarduser verwenden.
            $artikel1->setUid(1);
            $artikel1->setApublicationdate($_POST['releasedate']);
            $artikel1->create();

            //Umleiten auf Artikelübersicht...
            header('Location: index.php');
        }
        catch(Exception $ex)
        {
            //Todo: sinnvolle Fehlermeldungen ausgeben
        }

      

    }

?>

<div class="container">
    <div class="row">
        <h2>Beitrag erstellen</h2>
    </div>

    <form class="form-horizontal" action="create.php" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45" value="">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate" value="">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required">
                    <label class="control-label">Besitzer *</label>
                    <input type="text" class="form-control" name="owner" value="User 1" readonly >
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10"></textarea>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>
    <?php
    
    ?>
</div> <!-- /container -->
</body>
</html>