<?php
class Game
{
    private int $id;
    private string $titel;
    private float $preis;
    private string $kategorie;

    /**
     * Konstruktor für Games
     * @throws Exception Falls die Validierung der Parameter fehlschlägt
     */
    public function __construct($id, $title, $price, $genre)
    {
        //$this->id = $id;
        try{
            $this->setId($id);
            $this->setTitel($title);
            $this->setPreis($price);
            $this->setKategorie($genre);
        }
        catch (Exception $e)
        {
            throw $e;
        }
           
    }

    /**
     * Get the value of id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param int $id Die neue ID des Games
     * @return self
     * @Exception Falls die Validierung fehlgeschlagen ist.
     */
    public function setId(int $id): self
    {
        if ($id >= 0)
        {
            $this->id = $id;
            return $this;
        }
        else{
            //Fehlerbehandlung
            throw new Exception("ID muss positiv sein.");
        } 
    }

    /**
     * Get the value of titel
     *
     * @return string
     */
    public function getTitel(): string
    {
        return $this->titel;
    }

    /**
     * Set the value of titel
     *
     * @param string $titel
     *
     * @return self
     */
    public function setTitel(string $titel): self
    {
        $this->titel = $titel;

        return $this;
    }

    /**
     * Get the value of preis
     *
     * @return float
     */
    public function getPreis(): float
    {
        return $this->preis;
    }

    /**
     * Set the value of preis
     *
     * @param float $preis
     *
     * @return self
     */
    public function setPreis(float $preis): self
    {
        $this->preis = $preis;

        return $this;
    }

    /**
     * Get the value of kategorie
     *
     * @return string
     */
    public function getKategorie(): string
    {
        return $this->kategorie;
    }

    /**
     * Set the value of kategorie
     *
     * @param string $kategorie Die neue Kategorie des Games
     *
     * @return self
     */
    public function setKategorie(string $kategorie): self
    {
        $this->kategorie = $kategorie;

        return $this;
    }


    /**
     * Die Methode liefert alle Spiele zurück
     * @return array of Game Alle gefundenen Spiele
     */
    public static function getAllGames()
    {
      
        $games_string = file_get_contents('data/games.json');
        
        if ($games_string === false)
        {
            //Todo: Qualifizierte Fehlerbehandlung
            echo "Nix gut!";
        }
        else{
            //echo $games_string;
            $gameobjekte = json_decode($games_string);
            //var_dump($gameobjekte->games[0]->title);
            $zielsammlung = array();
            foreach ($gameobjekte->games as $key => $value) {
                //var_dump($value->title);
                $zielsammlung[]= new Game($value->id, $value->title, $value->price, $value->genre);
                //array_push($zielsammlung, new Game($value->id, $value->title, $value->price, $value->genre));
            }
            return $zielsammlung;
            
        }
   
        

    }
}

