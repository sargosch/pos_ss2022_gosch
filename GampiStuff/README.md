

---

>"JavaScript wird sich als Programmiersprache nie durchsetzen" [GAMP 2010]

---

# Mitschrift POS Wintersemester 2022

In der Mitschrift werden wichtige Unterrichtskonzepte notiert, ebenso währende der Bearbeitung der Arbeitsaufträge auftauchende Fragen.


## Übersicht der Projekte

* [Startseite](index.html)
* [0_HalloPHP](0_hellophp/index.php)
* [1_1_Formulare](1_formulare/index.php)
* [1_3_Benutzerdatenverwaltung](1_3_benutzerdatensuche/index.php)
* [2_1_Notenerfassung](2_1_notenerfassung/index.php)
* [2_2_Wochenkarte](2_2_wochenkarte/index.php)

## 20.9.2022: Einleitung PHP  - Teil 1
Kurze Einleitung in die Konzepte von PHP als Server-seitige Skriptsprache auf der Command Line und als Interpreter für eingebetteten Code in HTML-Dokumenten. 

![Überblick](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/PHP_funktionsweise.svg/780px-PHP_funktionsweise.svg.png?20121129070241)

Tags: HTTP-Request-Response, dynamische Inhalte mit serverseitiger Programmierung

Besprochene Konzepte:
- Schwache, dynamische Typisierung von PHP. Optionale Typangaben.
- Variablen (Definition, Wertzuweisung, Literalschreibweisen, Variablensubstitution)
- Fertige Funktionen verwenden (Doku lesen, call-by-value vs. call-by-reference)
- Eigene Funktionen definieren und aufrufen
- Einfache Klassen definieren und verwenden
- Standard-konforme Dokumentation schreiben
- Indizierte Arrays verwenden

````php
//Definieren von Variablen
$name = 'Michael';
echo "Username $name";

//Definition Mit Datentyp
String $nachname = 'Gamper';

/**
 * Diese Funktion bildet aus einem Vornamen und einem Nachnamen einen String
 * @param String Vorname
 * @param String Nachname
 * @param String Anrede Optionaler Parameter. Wenn nicht angegeben, ist die Anrede 'Herr'
 * @return String Fullname
 */
function getFullname($vname, $nname, $anrede='Herr')
{
    //TODO: Parameter validieren
    
    return $anrede .' ' . $vname.' '.$nname;
}

//Aufruf der Funktion
getFullname('Michael','Gamper')

````

## 27.9.2022: Einleitung PHP  - Teil 2

*Programm*
- Formularverarbeitung
- Datenübermittlung mit HTTP GET und POST
- Validierung am Client
- Validierung am Server

*Softskills*
- Responsive Design mit Bootstrap
- Versionsverwaltung und Kollaboration mit GIT
- Textauszeichnung mit MarkDown
- VSCode Shortcuts

## 4.10.2022: Beispiel Notenerfassung
- Durcharbeiten der Expertenlösung von Dominik Neuner
- Dokumentieren aller wichtigen Konzepte in der Mitschrift
- Durcharbeiten der zuätzlichen Ressourcen (nach Bedarf)

## 11.10.2022: Beispiel Notenerfassung - Teil 2
Neue Konzepte:
- Daten austauschen mit HTTP Sessions
- Einbinden von CSS und JS
- Bootstrap verwenden
- WH JavaScript-Grundlagen
- Client-seitige Validierung mit JavaScript



## 8.11.2022: Beispiel 1.3 - Benutzerdatensuche


Das erwartete Produkt: 

![Mockup Suche](1_3_benutzerdatensuche/angabe/mockup1.png)
![Mockup Userdetails](1_3_benutzerdatensuche/angabe/mockup2.png)

0) Angabe-Dateien verstehen
1) Server-Teil erstellen
2) Einfaches Suchformular erstellen (GET oder POST)
3) Server-seitig auf GET-/POST-Parameter reagieren

Der Funktion zum Zurückgeben der Benutzerdaten sollte mindestens folgendes umfassen:

- [x] Die Benutzerdaten sollen über entsprechende Funktionen zur Verfügung gestellt werden
    * function getAllData()
    * function getDataPerId($id)
    * function getFilteredData($filter)   

    
- [x] Eingabefeld zum Filtern nach Name und E-Mail
    
    * Tabellenzeilen werden entsprechend dem Suchfeld herausgefiltert
    * Eine Teilübereinstimmung des Suchtextes wird als Treffer gewertet
    * Buttons zum Suchen und Leeren 

- [x] Darstellung der wichtigsten Benutzerdaten in Tabellenform

    * Vorname und Nachname in gemeinsamer Spalte
    * Link auf die Detailseite mit Hilfe der Datensatz-ID zB. href=“details.php?id=1“
    *  Datumsformat DD.MM.YYYY
    * Zeilen farblich abwechselnd gestalten



- [x] Ausgabe einer Fehlermeldung falls keine Einträge gefunden wurden

- [x] Responsive Design. Verwendung eines HTML/CSS Frameworks, z.B. Bootstrap

Die Detailseite für Benutzerdaten sollte mindestens folgendes umfassen:

- [x] Übersichtliche Darstellung aller Benutzerdaten

- [x] Über einen zurück-Link wird der User wieder zur Übersichtsseite geleitet.


## 22.11.2022: Cookies, Sessions und OO

![Cookie](2_0_Cookies_Sessions/img/cookies.jpg)

Im Zuge der heutigen Sessions sollen die Grundlagen der folgenden Web-Technologien vermittelt werden:

* Cookies: Speichern von Infos am Client
* Sessions: Speichern von Infos am Server
* Anwendungsfälle für Cookies und Sessions aus der Praxis


### Cookies
Cookies sind kleine Textinfos (Key-Value-Pairs), die **am Client (Browser) gespeichert** werden und bei jedem HTTP-Request an den Server mitgeschickt werden.
![Cookie](2_0_Cookies_Sessions/img/cookies.drawio.png)

Server kann diese Info des Clients verwenden und darauf reagieren.

* HTTP-Protokoll kann mit einem Status versehen werden
* Server können Clients wiedererkennen
* Zum Beispiel für Speicherung von Benutzerwahl (Sprache, Stil, etc.)
* Wird meist zum Wiedererkennen des Browsers (und damit des Beutzers) verwendet. Z.B. für personalisierte Werbung, etc.
* Sind damit als nicht vertrauenswürdige Daten vom Client zu behandeln (-> validieren)

1) Server setzt Cookie mit Inhalt und Gültigkeitsdauer
2) Client empfängt und speichert das Cookie
3) Client sendet dasselbe Cookie bei jedem Request an den Server mit.
4) Client löscht Cookies, wenn die Gültigkeitsdauer überschritten ist.
5) User kann Cookies auch manuell löschen.


### Session
![Sessions](2_0_Cookies_Sessions/img/sessions.drawio.png)
Sessions speichern Infos als Key-Value-Pairs **Server-seitig**.
* Session-Cookies werden Client-seitig gesetzt.
* Session-Cookies werden nur für die Dauer der Session behalten.
* Session-Cookies speichern für jeden Client einen eindeutige ID
* Die ID wird auch am Server gespeichert, zusätzlich aber auch beliebig viele Daten zu dieser Session.


#### Login mit Hilfe von Sessions realisieren

1) Login-Formular herrichten
2) Login daten prüfen
3) Session befüllen
4) Benutzerrollen mit Hilfe der Session verwalten
5) Fehlermeldungen


## 05.12.2022: Objektorientierung


Die meisten umfangreichen Projekte sind objekt-orientiert aufgebaut, was die Erweiterbarkeit, Wartbarkeit und Wiederverwendbarkeit fördert.

Die OO-Prinzipien aus Java gelten unverändert auch für die OO-Programmierung mit PHP.

Übersicht der OO:

* Klassen mit Instanzvariablen und Methoden
* Konstruktoren, Getter- und Setter-Methoden
* Setter-Methoden zur Validierung verwenden
* Vererbung und Polymorphie möglich
* Interfaces möglich
* Statische Variablen und Methoden

In der Webentwicklung wird OO meist beim Verwenden fertiger Klassensammlungen (APIs/Libraries) und Frameworks verwendet.

In PHP speziell: Instanz-Operator -> und Klassen-Operator :: zum Aufruf von Eigenschaften und Methoden. *this* als Referenz auf das Objekt, *self* als Referenz auf die Klasse.

Ein sehr gängiges Designpattern ist das Model-View-Controler Entwurfsmuster, das eine Webanwendung in drei Teile trennt:

1) Model: Bildet die Domäne/die Business-Logik samt Datenhaltung ab.
2) View: Kümmet sich um die Darstellung (lädt z.B. Templates und befüllt diese mit Werten).
3) Controller: Hängt die beiden Teile zusammen. Für jede Domäne existiert ein Controller, der die wichtigsten Methoden zum arbeiten beinhaltet. Er hängt das passende Model mit den passenden Views zusammen.

In unserem ersten Teil der OO kümmern wir uns um die Model-Klassen. Diese bilden die Domäne ab. So hätte ein Webshop z.B. die Model-Klassen "Artikel" und "User.

Jede Model-Klasse weiß, wie sie mit den eigenen Daten umzugehen hat. So kann zum Beispiel die Artikel-Klasse neue Artikel anlegen, bestehende verändern und Daten zu den Artikeln liefern (über die Konstruktoren, Getter- und Setter-Methoden).

Zusätzlich weiß die Klasse, wie Daten persistiert werden können. Sie bildet die sogenannten CRUD-Metoden zum Zugriff auf z.B. eine Datenbank ab:
* C reate: Speichern eines neuen Artikels in der DB.
* R ead: Auslesen eines oder mehrerer Artikel aus der DB.
* U pdate: Verändern bestehender Artikel in der DB.
* D elete: Löschen bestehender Artikel aus der DB.

In unserem Beispiel haben wir noch keine DB zur Verfügung, deshalb könnten wir unsere GradeEntries in eine Session oder ein File speichern.

Die beiden Methoden zum Auslesen aus einer Datenquelle müssen statisch sein und liefern ein Objekt der Klasse zurück (da ja beim Auslesen noch kein sinnvolles GradeEntry-Objekt existiert).

![UML Klassendiagramm](2_1_notenerfassung/img/uml.drawio.png)


## 20.12.2022: Objektorientierung am Beispiel Wochenkarte

Rapid Development des Frontends mit Hilfe von Bootstrap (und Vorlagen):
1) Vorlage mit Modal (für Cookie-Banner), Login-Formular und einigen Einträgen
2) Bei Bestätigen des Cookie-Banners: Seite anzeigen, ansonsten soll das Modal stehenbleiben.
3) Beim Absenden des Login-Formulars an index.php, soll die Benutzer-Klasse verwendet werden, um:
4) Einen Benutzer zu "holen" mit Hilfe der get-Methode
5) Die Session-Infos zu setzen mit Hilfe der login-Methode
6) Die Session-Infos zu prüfen mit Hilfe der isLoggedIn-Methode
7) Die Session zurückzusetzen mit Hilfe der logout-Methode

Fokus der Übung ist also die Erstellung der Klasse *Benutzer*:

![UML Benutzer](2_2_wochenkarte/img/UML_Userclass.drawio.png)

## 28.02.2023: Datenbankzugriff am Beispiel PW-Manager

0) Rohen Datenbankzugriff mit PDO verstehen
1) Projektstruktur verstehen
2) Credentials-Klasse verstehen (Model)
3) Active Record Pattern verstehen
4) CRUD-Ansichten verstehen (Views zum Model)

### Datenbank-Zugriff über PDO oder myslqi

PDO = PHP Database Objects
Ist eine API zum Ansprechen von relationalen DBMS. Kann im Vergleich zu mysqli mit verschiedenen DBMS sprechen und ist streng Objekt-orientiert zu verwenden.

Ein direkter Zugriff auf die DB-Tabelle ist möglich, wird aber nicht empfohlen.

![Roher Datenbank-Zugriff](3_1_PWManager/img/dbaccess.drawio.png)

State-of-the-art ist der Zugriff über Prepared Statements. Dabei wird eine Query mit Platzhaltern versehen und dem DBMS gesendet. Dieses kann die notwendigen Tabellen vorbereiten (prepare). Im nächsten Schritt werden den Platzhaltern konkrete Werte zugewiesen. Erst danach wird die Query tatsächlich ausgeführt, wobei die Werte immer nur als Werte eines bestimmten Typs interpretiert werden (niemals aber als SQL-Befehle, siehe SQL Injection).

Vorteile:
* Performance (vor allem, wenn mehrere ähnliche Statements abgesetzt werden)
* Sicherheit

![Prepared Statements](3_1_PWManager/img/dbaccess_prepared.drawio.png)


### Projektstruktur verstehen

Das Projekt ist gleich aufgebaut, wie sonst in der OO auch: Die Model-Klassen bilden die Daten ab. Statt die Daten aus einem File zu laden, holen wir diese aus der Datenbank.

Dazu "zwingen" wir die Model-Klassen zum Implementieren der CRUD-Methoden aus dem Interface. So können wir sicherstellen, dass alle Model-Klassen dieselben CRUD-Methoden anbieten.

Um Code-Duplizierung zu vermeiden, wird die DB-Verbindung in eine eigene Hilfsklasse ausgelagert.

![UML Passwort-Manager](3_1_PWManager/img/uml_PWManager.png)

Im Active Record - Entwurfsmuster erledigt die Model-Klasse auch den Datenbankzugriff und implementiert damit die CRUD-Methoden aus dem Interface.

Die Methoden bilden das sogenannte Objekt-relationale Mapping ab: Eine Tabellenzeile der Tabelle X wird ein Objekt der Klasse X.



# Crashkurs POS

Gemeinsames Coden wesentlicher Konzepte. Gitlab-Repository, Teams und Live Share als Tools.

## Termine
* 21.11.2022 - 11 Uhr
* 5.12.2022 - 11 Uhr

## Mögliche Inhalte

* PHP-Grundlagen
* GET/POST
* HTML/Bootstrap
* Herangehensweise, coding techniques
* best practices, Konventionen


