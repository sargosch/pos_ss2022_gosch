<?php
    session_start();
?>

<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <?php
     
     ?>

    

    <title>Cookies!!!</title>
</head>
<body>
    <nav class="navbar bg-primary">
    <div class="container-fluid">
        <h1><a class="text-light">Cookies!!!.at</a></h1>
        <?php
        if(isset($_SESSION['eingeloggt']) && $_SESSION['eingeloggt']==true)
        {
            echo '<a class="btn btn-secondary" href="php/logout.php">Logout</a>';
        }
        else{
            if(isset($_SESSION['fehler']) && $_SESSION['fehler']==true)
            {
                $uname = isset($_SESSION['username'])?$_SESSION['username']:"";
                $formHTML = '<form class="d-flex" role="login" method="POST" action="php/login.php">
                <input class="form-control me-2 is-invalid" type="text" name="username" placeholder="Benutzername" aria-label="username" value="'. $uname .'">
                <input class="form-control me-2 is-invalid" type="password" name="passwort" placeholder="********" aria-label="password">
                <button class="btn btn-secondary" type="submit">Login</button>
                </form>';
                echo $formHTML;
            }
            else{
                echo ' <form class="d-flex" role="login" method="POST" action="php/login.php">
                <input class="form-control me-2" type="text" name="username" placeholder="Benutzername" aria-label="username">
                <input class="form-control me-2" type="password" name="passwort" placeholder="********" aria-label="password">
                <button class="btn btn-secondary" type="submit">Login</button>
                </form>';
            }
               
        }
        ?>
       
    </div>
    </nav>
    <header class="container-fluid h-25" style="background-image: url('img/cookies.jpg'); background-repeat: none; background-size: cover;"></header>
    <section class="container-fluid">
        <?php
            if(isset($_SESSION['eingeloggt']) && $_SESSION['eingeloggt']==true)
            {
                $htmlcode = '<p>Some secret cookie recipies.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi obcaecati numquam itaque nam tempora natus enim maiores, reiciendis veritatis adipisci debitis! Beatae sequi dolorum, sapiente magnam voluptatum possimus voluptatibus, officia labore itaque enim deleniti nam ex deserunt blanditiis tempore illum culpa amet quia est optio laboriosam facere commodi aspernatur laborum.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi obcaecati numquam itaque nam tempora natus enim maiores, reiciendis veritatis adipisci debitis! Beatae sequi dolorum, sapiente magnam voluptatum possimus voluptatibus, officia labore itaque enim deleniti nam ex deserunt blanditiis tempore illum culpa amet quia est optio laboriosam facere commodi aspernatur laborum.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi obcaecati numquam itaque nam tempora natus enim maiores, reiciendis veritatis adipisci debitis! Beatae sequi dolorum, sapiente magnam voluptatum possimus voluptatibus, officia labore itaque enim deleniti nam ex deserunt blanditiis tempore illum culpa amet quia est optio laboriosam facere commodi aspernatur laborum.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi obcaecati numquam itaque nam tempora natus enim maiores, reiciendis veritatis adipisci debitis! Beatae sequi dolorum, sapiente magnam voluptatum possimus voluptatibus, officia labore itaque enim deleniti nam ex deserunt blanditiis tempore illum culpa amet quia est optio laboriosam facere commodi aspernatur laborum.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi obcaecati numquam itaque nam tempora natus enim maiores, reiciendis veritatis adipisci debitis! Beatae sequi dolorum, sapiente magnam voluptatum possimus voluptatibus, officia labore itaque enim deleniti nam ex deserunt blanditiis tempore illum culpa amet quia est optio laboriosam facere commodi aspernatur laborum.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi obcaecati numquam itaque nam tempora natus enim maiores, reiciendis veritatis adipisci debitis! Beatae sequi dolorum, sapiente magnam voluptatum possimus voluptatibus, officia labore itaque enim deleniti nam ex deserunt blanditiis tempore illum culpa amet quia est optio laboriosam facere commodi aspernatur laborum.</p>
                ';
                
                echo $htmlcode;
            }
        ?>

    </section>
    <footer class="container-fluid bg-primary text-light position-sticky bottom-0 text-center">&copy; Michael Gamper 2022 | <a class="link-secondary" href="?theme=light">Light Theme</a> | <a class="link-secondary" href="?theme=dark">Dark Theme</a></footer>
</body>
</html>