<?php
    session_start();

    //Super geheime Logininfos (sind am Server gespeichert)
    $username_secret = 'admin';
    $pw_secret = 'Testitest4!';

    //Prüfe, ob der User berechtigt zum Login ist
    if (isset($_POST['username']) && isset($_POST['passwort']))
    {
        if ($_POST['username'] == $username_secret && $_POST['passwort'] == $pw_secret)
        {
            //Schreibe Info in die Session
            $_SESSION['eingeloggt'] = true;
        }
        else
        {
            $_SESSION['fehler'] = true;
            //Todo: Vor Ausgabe username validieren
            $_SESSION['username'] = htmlspecialchars($_POST['username']);
        }
        header('Location: ../index.php');
    }
    else{
        //echo "Keine Formulardaten vorhanden.";
    }
?>