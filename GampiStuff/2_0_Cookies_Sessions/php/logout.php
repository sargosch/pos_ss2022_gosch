<?php
    //Hole aktive Session
    session_start();

    //Setze Session-Cookie beim Browser in die Vergangenheit (wird vom Browser gelöscht)
    setcookie(session_name(),'',time()-42000,'/','',false,true);
    
    //Zerstöre die Session-Infos am Sever
    session_destroy();

    //Leite um...
    header('Location: ../index.php');
?>
